<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Naijaways</title>
    <link
            href="https://fonts.googleapis.com/css?family=Barlow&ampdisplay=swap"
            rel="stylesheet"
    />
    <style>
        body {
            background-color: #e5e5e5;
            overflow: hidden;
            font-family: "Barlow", sans-serif;
            color: black;
            display: grid;
            place-items: center;
        }
        a {
            text-decoration: none;
            color: inherit;
            vertical-align: middle;
            display: inline-block;
            margin: 0;
        }

        .container {
            width: 35%;
            margin: 5% auto;
            background-color: white;
            border-radius: 1vw;
            padding: 4em 2em;
            border: none;
        }

        .container__secondary {
            border-radius: 1vw;
            overflow: hidden;
            background-color: #fafafa;
        }

        .alerzo-logo {
            display: block;
            margin: auto;
        }

        .heading {
            text-transform: capitalize;
            text-align: center;
            font-weight: bold;
            color: #374b58;
            background-color: #eff7ff;
            padding: 20px;
            font-size: larger;
        }
        .message-body {
            padding: 5px 25px;
        }
        .message-body * {
            margin: 25px 0;
        }

        .cta {
            text-align: center;
            border-radius: 5px;
        }
        footer {
            text-align: center;
            margin-top: 15px;
            padding: 15px 0;
            background-color: white;
        }
        footer p {
            margin: 0%;
        }

        .text-sm {
            font-size: small;
        }

        .text-b {
            font-weight: bold;
        }
        .text-b-2x {
            font-weight: 900;
        }

        .text-grey {
            color: #374b58;
        }
        .text-grey-100 {
            color: #9ca8af;
        }
        .text-l {
            font-weight: light;
        }
        .accept-invitation {
            border-radius: 5px;
            padding: 10px 15px;
            color: white;
            font-weight: bolder;
            text-align: center;
            text-transform: uppercase;
            font-size: small;
            background-color: #0077ff;
        }

        @media screen and (max-width: 400px) {
            body {
                padding: 0%;
                margin: 0%;
                display: block;
            }
            .container {
                width: 100%;
            }
        }
        @media screen and (max-width: 1000px) {
            .container {
                width: 80%;
            }
        }
    </style>
</head>
<body>
<main class="container">
    <img src="124x21.png" alt="logo" class="alerzo-logo" />
    <section class="container__secondary">
        <header class="heading">Password Reset</header>
        <div class="message-body">
            <p class="text-sm text-b">Dear {firstName} {lastName},</p>

            <p class="">
                A request has been made to reset your password on your account.
            </p>

            <div class="cta">
                <a
                        href={verificationLink}
                        class="accept-invitation"
                        target="_blank"
                        rel="noopener noreferrer"
                >
                    reset password
                </a>
            </div>

            <small class="text-grey-100 foot-note text-l">
                If you did not request to reset your password, please ignore this
                message
            </small>
        </div>
        <footer>
            <p>
                <small
                ><span class="text-grey-100">Powered by:</span>
                    <span class="text-grey">Naijaways</span></small
                >
            </p>
            <small
            >Victoria Island, Lagos, Nigeria</small
            >
        </footer>
    </section>
</main>
</body>
</html>Ï

package com.naijaways.repository;

import com.naijaways.entity.Permission;
import com.naijaways.entity.Role;
import com.naijaways.enums.PermissionName;
import com.naijaways.enums.PermissionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Long> {
        List<Permission> findPermissionsByRoles(Role role);

        List<Permission> findAllByPermissionTypeIn(List<PermissionType> admin);

        Optional<Permission> findByName(PermissionName name);
}

package com.naijaways.repository;

import com.naijaways.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
        User findUserByEmail(String email);

        User findByUsername(String username);

        boolean existsByEmail(String email);
}

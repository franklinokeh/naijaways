package com.naijaways.repository;

import com.naijaways.entity.Role;
import com.naijaways.enums.RoleType;
import io.grpc.MethodDescriptor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository

public interface RoleRepository extends JpaRepository<Role, Long> {

        Collection<Role> findAllByBusinessId(Long businessId);

        Collection<Role> findAllByRoleType(RoleType type);

        Optional<Role> findByNameAndRoleType(String name, RoleType type);

        Optional<Role> findByNameAndRoleTypeAndBusinessId(String name, RoleType type, Long businessId);

        Optional<Role> findByIdAndBusinessId(Long roleId, Long businessId);

        @Query("SELECT r FROM Role r WHERE r.roleType = 'BUSINESS' AND r.createdBy IS NULL")
        List<Role> findBusinessRolesWithoutCreatedBy();

}

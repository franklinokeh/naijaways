package com.naijaways.repository;


import com.naijaways.entity.BusinessUser;
import com.naijaways.entity.Role;
import com.naijaways.models.PageableDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Slf4j
public class BusinessUserEntityManagerRepo {
        @PersistenceContext
        private EntityManager entityManager;


        public List<BusinessUser> getAllBusinessUsersAdmin(PageableDto pageableDto, Role role) {
                int pageNumber = pageableDto.getPage();
                int pageSize = pageableDto.getPageSize();
                try {
                        return entityManager.createNativeQuery("SELECT * FROM business_users bur INNER JOIN roles rl on bur.role_id = rl.id WHERE rl.name =:roleName AND rl.id =:roleId", BusinessUser.class)
                                .setParameter("roleName", role.getName())
                                .setParameter("roleId", role.getId())
                                .setMaxResults(pageSize)
                                .setFirstResult((pageNumber) * pageSize)
                                .getResultList();
                } catch (Exception e) {
                        log.error("repo--{}", e.getMessage());
                        return null;
                }
        }
}

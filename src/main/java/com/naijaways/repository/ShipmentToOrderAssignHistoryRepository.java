package com.naijaways.repository;

import com.naijaways.entity.ShipmentToOrderAssignHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentToOrderAssignHistoryRepository extends JpaRepository<ShipmentToOrderAssignHistory, Long> {
}

package com.naijaways.repository;

import com.naijaways.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long> {
        Admin findAdminByUserId(Long id);
}

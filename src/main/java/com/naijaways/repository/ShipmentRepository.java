package com.naijaways.repository;

import com.naijaways.entity.Shipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShipmentRepository extends JpaRepository<Shipment, Long> {
        Shipment getShipmentByTrackingNo(String shipmentTrackingNo);
}

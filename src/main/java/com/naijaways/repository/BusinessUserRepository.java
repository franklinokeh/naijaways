package com.naijaways.repository;

import com.naijaways.entity.Business;
import com.naijaways.entity.BusinessUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BusinessUserRepository extends JpaRepository<BusinessUser, Long> {
        BusinessUser findBusinessUserByUserId(Long id);

        @Query(value = "SELECT * FROM business_users bur INNER JOIN users ur on bur.user_id = ur.id " +
                "WHERE ur.email = ?1", nativeQuery = true)
        Optional<BusinessUser> getBusinessUserByEmail(String email);

        @Query(value = "SELECT * FROM business_users bur INNER JOIN business bs on bur.business_id = bs.id INNER JOIN roles rl on bur.role_id = rl.id " +
                "WHERE bs.id = ?1 AND rl.name = ?2 order by bs.created_date desc", nativeQuery = true)
        Optional<BusinessUser> getBusinessUserByBusinessIdAndRoleName(Long businessId, String roleName);

        List<BusinessUser> findAllByBusiness(Business business);
}

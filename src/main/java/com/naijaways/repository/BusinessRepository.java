package com.naijaways.repository;

import com.naijaways.entity.Business;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BusinessRepository extends JpaRepository<Business, Long> {
        Optional<Business> findBusinessByName(String name);

        @Query(value = "UPDATE users ur SET non_locked = false FROM business_users bur WHERE ur.id = bur.user_id AND bur.business_id =:id RETURNING ur.email", nativeQuery = true)
        List<String> disableBusiness(Long id);

        @Query(value = "UPDATE users ur SET non_locked = true FROM business_users bur WHERE ur.id = bur.user_id AND bur.business_id =:id RETURNING ur.email", nativeQuery = true)
        List<String> enableBusiness(Long id);
}

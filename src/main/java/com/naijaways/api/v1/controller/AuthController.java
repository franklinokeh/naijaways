package com.naijaways.api.v1.controller;

import com.naijaways.apis.AuthApi;
import com.naijaways.exception.AuthException;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.ChangePasswordDto;
import com.naijaways.models.VerifyUserDto;
import com.naijaways.service.auth.AuthService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@CrossOrigin(origins = "*")
public class AuthController implements AuthApi {

        AuthService authService;


        @Override
        public ResponseEntity<ApiDataResponseDto> _changePassword(ChangePasswordDto changePasswordDto) {
                if (!changePasswordDto.getRequest().getPassword().equals(changePasswordDto.getRequest().getConfirmPassword()))
                        throw new AuthException("Passwords don't match");
                return ResponseEntity.ok(
                        authService.changePassword(changePasswordDto)
                );
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _sendPasswordResetMail(String email) {
                return ResponseEntity.ok(
                        authService.initiatePasswordReset(email)
                );
        }
        @Override
        public ResponseEntity<ApiDataResponseDto> _verifyUser(String token, VerifyUserDto verifyUserDto) {
                return null;
        }
}

package com.naijaways.api.v1.controller;

import com.naijaways.apis.OrderApi;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.CreateOrderDto;
import com.naijaways.models.PageableDto;
import com.naijaways.models.SortOrdersDto;
import com.naijaways.service.order.OrderService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class OrderController implements OrderApi {

        @Autowired OrderService orderService;


        @Override
        public ResponseEntity<ApiDataResponseDto> _createOrder(CreateOrderDto createOrderDto) {
                return orderService.createOrder(createOrderDto);
        }

        @SneakyThrows
        @Override
        public ResponseEntity<ApiDataResponseDto> _createOrderByWaybill(MultipartFile file) {
                return orderService.createOrderByWaybill(file);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _getOrders(SortOrdersDto pageable) {
                return orderService.getAllOrders(pageable);
        }

}

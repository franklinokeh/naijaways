package com.naijaways.api.v1.controller;

import com.naijaways.apis.ReviewApi;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.CreateReviewDto;
import com.naijaways.models.PageableDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class ReviewController implements ReviewApi {
        @Override
        public ResponseEntity<ApiDataResponseDto> _createReview(CreateReviewDto createReviewDto) {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _getReviews(PageableDto pageable) {
                return null;
        }
}

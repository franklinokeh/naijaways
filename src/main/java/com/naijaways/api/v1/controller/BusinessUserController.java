package com.naijaways.api.v1.controller;

import com.naijaways.apis.BusinessUserApi;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.UserDto;
import com.naijaways.models.UserRegisterDto;
import com.naijaways.service.user.businessUser.BusinessUserService;
import lombok.AllArgsConstructor;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class BusinessUserController implements BusinessUserApi {
        BusinessUserService businessUserService;

        @Override
        public ResponseEntity<ApiDataResponseDto> _createBusinessUser(UserRegisterDto businessUserRegisterDto) {
                return ResponseEntity.ok(businessUserService.createBusinessUser(businessUserRegisterDto));
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _getBusinessUsers() {
                return ResponseEntity.ok(businessUserService.getBusinessUsers());
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _updateBusinessUser(UserDto userDto) {
                return ResponseEntity.ok(businessUserService.updateBusinessUser(userDto));
        }
}

package com.naijaways.api.v1.controller;

import com.naijaways.apis.BusinessApi;
import com.naijaways.entity.BusinessUser;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.BusinessProfileDto;
import com.naijaways.models.CreateBusinessDto;
import com.naijaways.models.UpdateBusinessDto;
import com.naijaways.service.business.BusinessService;
import com.naijaways.utils.EmailTemplateSender;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;


@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class BusinessController implements BusinessApi {
        private final BusinessService businessService;
        private final EmailTemplateSender templateSender;

        public ResponseEntity<ApiDataResponseDto> _createBusiness(CreateBusinessDto createBusinessDto) {
                try {
                        BusinessUser businessUser = businessService.createBusiness(createBusinessDto);
                        templateSender.sendUserInviteMail(businessUser.getUser());
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 201, null, "A " +
                                "verification email has been sent to: " + businessUser.getUser().getEmail(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.CREATED);
                }catch (Exception e){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 400, null,
                                e.getMessage(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.BAD_REQUEST);
                }
        }


        public ResponseEntity<ApiDataResponseDto> _disableBusiness(Long businessId) {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _enableBusiness(Long businessId) {
                return null;
        }


        @Override
        public ResponseEntity<ApiDataResponseDto> _registerBusiness(CreateBusinessDto createBusinessDto) {
                try {
                        BusinessUser businessUser = businessService.createBusiness(createBusinessDto);
                        templateSender.sendUserInviteMail(businessUser.getUser());
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 201, null, "A " +
                                "verification email has been sent to: " + businessUser.getUser().getEmail(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.CREATED);
                }catch (Exception e){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 400, null,
                                e.getMessage(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.BAD_REQUEST);
                }
        }


        @Override
        public ResponseEntity<ApiDataResponseDto> _updateBusiness(Long businessId, UpdateBusinessDto updateBusinessDto) {
                try {
                        businessService.updateBusiness(businessId, updateBusinessDto);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, null, "Successfully updated business", LocalDateTime.now());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.CREATED);
                }catch (Exception e){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 400, null, e.getMessage(), LocalDateTime.now());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.BAD_REQUEST);
                }
        }
        @Override
        @PreAuthorize("hasAuthority('BUSINESS_VIEW')")
        public ResponseEntity<ApiDataResponseDto> _getAllBusiness(Integer page, Integer pageSize) {
                try {
                        List<BusinessProfileDto> businessProfileDtos = businessService.getAllBusiness(page, pageSize);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, businessProfileDtos, "Successfully retrieved business profiles", LocalDateTime.now());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
                }catch (Exception e){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 404, null, e.getMessage(), LocalDateTime.now());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
                }
        }

        @Override
        @PreAuthorize("hasAuthority('BUSINESS_VIEW')")
        public ResponseEntity<ApiDataResponseDto> _getBusiness(Long businessId) {
                try {
                        BusinessProfileDto businessProfileDto = businessService.getBusiness(businessId);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, businessProfileDto, "Successfully retrieved business profiles", LocalDateTime.now());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
                }catch (Exception e){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 404, null, e.getMessage(), LocalDateTime.now());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
                }
        }




}

package com.naijaways.api.v1.controller;

import com.naijaways.apis.UserApi;
import com.naijaways.config.securityConfig.JWTUtility;
import com.naijaways.entity.User;
import com.naijaways.models.*;
import com.naijaways.service.MyUserDetailsService;
import com.naijaways.service.user.UserService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class UserController implements UserApi {

        private final MyUserDetailsService myUserDetailsService;
        private final AuthenticationManager authenticationManager;
        private final UserService userService;
        private final JWTUtility jwtUtility;

        @Override
        public ResponseEntity<ApiDataResponseDto> _disableUser(String username) {
                try {
                        userService.disableUser(username);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, null, username + " " +
                                "has been disabled", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
                }catch (Exception e){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 400, null, e.getMessage(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.BAD_REQUEST);
                }
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _enableUser(String username) {
                try {
                        userService.enableUser(username);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, null, username + " has been enabled", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
                }catch (Exception e){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 400, null, e.getMessage(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.BAD_REQUEST);
                }
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _getAllUsers(Integer page, Integer pageSize) {
                Pageable pageable = PageRequest.of(page, pageSize);
                List<UserDto> userDtoList = new ArrayList<>();
                Page<User> userList = userService.getAllUsers(pageable);

                for (User user : userList){
                        UserDto userDto = userService.userToUserDto(user);
                        userDtoList.add(userDto);
                }

                PageableResponseDto data = PageableResponseDto.builder()
                        .totalPages(userList.getTotalPages())
                        .totalElements((int) userList.getTotalElements())
                        .pageNumber(userList.getNumber())
                        .size(userList.getSize())
                        .content(userDtoList)
                        .build();
                ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, data, "Successfully retrieved users", LocalDateTime.now().toString());
                return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _getLoggedInUser() {
                User user = userService.getLoggedInUser();
                if(user == null){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false,404,
                                userService.getLoggedInUser(), "User not found", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
                }
                ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true,200,
                        userService.getLoggedInUser(), "User found", LocalDateTime.now().toString());
                return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
        }

//        @Override
//        public ResponseEntity<ApiDataResponseDto> _loginUserForMobile(LoginRequestDto loginRequestDto) {
//                try {
//                        myUserDetailsService.loadUserByUsername(loginRequestDto.getEmail());
//                } catch (Exception e) {
//                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 404, null, e.getMessage(), LocalDateTime.now().toString());
//                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
//                }
//                Authentication authenticate;
//                try {
//                        authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken
//                                (loginRequestDto.getEmail(), loginRequestDto.getPassword()));
//                } catch (BadCredentialsException e) {
//                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 404, null, e.getMessage(), LocalDateTime.now().toString());
//                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
//                }
//                SecurityContextHolder.getContext().setAuthentication(authenticate);
//                ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, null, "user successfully login", LocalDateTime.now().toString());
//                return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
//        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _loginUserForWeb(LoginRequestDto loginRequestDto) {
                UserDetails userDetails = null;
                try {
                        userDetails = myUserDetailsService.loadUserByUsername(loginRequestDto.getEmail());
                } catch (Exception e) {
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 404, null, e.getMessage(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
                }
                Authentication authenticate;
                try {
                        authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken
                                (loginRequestDto.getEmail(), loginRequestDto.getPassword()));
                } catch (BadCredentialsException e) {
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 404, null, e.getMessage(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
                }
                SecurityContextHolder.getContext().setAuthentication(authenticate);
                final String token = jwtUtility.generateToken(userDetails);
                LoginResponseDto loginResponseDto = userService.getLoginRequestDto(loginRequestDto.getEmail(), token);
                ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, loginResponseDto, "user " +
                        "successfully login", LocalDateTime.now().toString());
                return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
        }



}

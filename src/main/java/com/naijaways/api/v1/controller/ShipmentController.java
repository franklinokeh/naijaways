package com.naijaways.api.v1.controller;

import com.naijaways.apis.ShipmentApi;
import com.naijaways.models.*;
import com.naijaways.service.shipment.ShipmentService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class ShipmentController implements ShipmentApi {

        @Autowired
        ShipmentService shipmentService;

        @Override
        public ResponseEntity<ApiDataResponseDto> _assignShipmentToOrder(AssignShipmentDto assignShipmentDto) {
                return shipmentService.assignShipmentToOrder(assignShipmentDto);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _createShipments(CreateOrderDto createOrderDto) {
                return shipmentService.createShipments(createOrderDto);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _getShipments(SortOrdersDto pageable) {
                return shipmentService.getShipments(pageable);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _getShipmentsSummary(ShipmentSummaryDto shipmentSummary) {
                return shipmentService.getShipmentSummary(shipmentSummary);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _updateShipment(UpdateShipmentDto updateShipmentDto) {
                return shipmentService.updateShipment(updateShipmentDto);
        }


}

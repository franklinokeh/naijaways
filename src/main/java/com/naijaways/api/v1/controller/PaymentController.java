package com.naijaways.api.v1.controller;

import com.naijaways.apis.PaymentApi;
import com.naijaways.entity.Payment;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.PageableDto;
import com.naijaways.repository.PaymentRepository;
import com.naijaways.service.payment.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class PaymentController implements PaymentApi {

        PaymentService paymentService;

        @Override
        public ResponseEntity<ApiDataResponseDto> _getPayments(PageableDto pageable) {
             return paymentService.getPayments(pageable);
        }
}

package com.naijaways.api.v1.controller;

import com.naijaways.apis.AdminUserApi;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.UserDto;
import com.naijaways.models.UserRegisterDto;
import com.naijaways.service.user.adminUser.AdminUserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class AdminUserController implements AdminUserApi {

        private final AdminUserService adminUserService;
        @Override
        public ResponseEntity<ApiDataResponseDto> _createAdminUser(UserRegisterDto userRegisterDto) {
                return ResponseEntity.ok(adminUserService.createAdminUser(userRegisterDto));
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _getAdminUsers() {
                return ResponseEntity.ok(adminUserService.getAdminUsers());
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _updateAdminUser(UserDto userDto) {
                return ResponseEntity.ok(adminUserService.updateAdminUser(userDto));
        }
}

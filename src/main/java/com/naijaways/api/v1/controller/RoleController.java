package com.naijaways.api.v1.controller;

import com.naijaways.apis.RolesApi;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.CreateRoleDto;
import com.naijaways.models.RoleDto;
import com.naijaways.service.role.RoleService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
@Slf4j
public class RoleController implements RolesApi {

        RoleService roleService;

        @Override
        public ResponseEntity<ApiDataResponseDto> _addPermissionToRole(Long roleId, List<Long> requestBody) {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _adminRoles() {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _businessDefaultRoles() {
                List<RoleDto> roles = roleService.getBusinessDefaultRoles();
                log.info("Business roles ... {}", roles);
                ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 00, roles, "Business roles " +
                        "received ", LocalDateTime.now().toString());
                return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _businessRoles(Long businessId) {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _createAdminRole(CreateRoleDto createRoleDto) {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _createBusinessRole(Long businessId, CreateRoleDto createRoleDto) {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _removeRolePermission(Long roleId, List<Long> requestBody) {
                return null;
        }
}

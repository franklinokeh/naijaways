package com.naijaways.api.v1.controller;

import com.naijaways.apis.PermissionsApi;
import com.naijaways.models.ApiDataResponseDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class PermissionController implements PermissionsApi {
        @Override
        public ResponseEntity<ApiDataResponseDto> _adminPermissions() {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _adminPermissionsByRoles(Long roleId) {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _businessPermissions() {
                return null;
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> _businessPermissionsByBusinessRoles(Long businessId, Long roleId) {
                return null;
        }
}

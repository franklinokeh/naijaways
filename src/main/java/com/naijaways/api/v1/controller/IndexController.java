package com.naijaways.api.v1.controller;

import com.naijaways.apis.IndexApi;
import com.naijaways.models.ApiDataResponseDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@CrossOrigin(origins = "*")
public class IndexController implements IndexApi {
        @Override
        public ResponseEntity<ApiDataResponseDto> _indexPath() {
                return null;
        }
        //
}

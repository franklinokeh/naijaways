package com.naijaways.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Misc {

    public static String generateRef() {
        return RandomStringUtils.randomAlphanumeric(12).toUpperCase();
    }


    public static boolean phoneNumberValidation(String phoneNumber){
        String phoneRegexp = "(080|081|090|091|070|071)\\d{8}";
        Pattern phonePattern = Pattern.compile(phoneRegexp);
        Matcher phoneMatcher = phonePattern.matcher(phoneNumber);
        return phoneMatcher.matches();
    }
}

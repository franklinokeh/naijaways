package com.naijaways.utils;

import com.naijaways.models.ApiDataResponseDto;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class DataResponseUtils {
        public static ApiDataResponseDto successResponse(String message, Object data) {
                return ApiDataResponseDto.builder()
                        .data(data)
                        .timestamp(LocalDateTime.now().toString())
                        .message(message)
                        .status(true)
                        .code(HttpStatus.OK.value())
                        .build();
        }

        public static ApiDataResponseDto successResponse(String message) {
                return ApiDataResponseDto.builder()
                        .timestamp(LocalDateTime.now().toString())
                        .message(message)
                        .status(true)
                        .code(HttpStatus.OK.value())
                        .build();
        }

        public static ApiDataResponseDto errorResponse(Throwable throwable) {
                String message = throwable.getCause().getMessage();
                return errorResponse(message);
        }

        public static ApiDataResponseDto errorResponse(String message) {
                return ApiDataResponseDto.builder()
                        .timestamp(LocalDateTime.now().toString())
                        .message(message)
                        .status(false)
                        .code(HttpStatus.BAD_REQUEST.value())
                        .build();
        }

        public static ApiDataResponseDto errorResponse(HttpStatus status, String message) {
                ApiDataResponseDto responseDto = errorResponse(message);
                responseDto.setCode(status.value());
                return responseDto;
        }
}

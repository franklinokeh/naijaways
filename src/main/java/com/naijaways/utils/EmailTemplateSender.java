package com.naijaways.utils;

import com.naijaways.config.EmailSender;
import com.naijaways.config.securityConfig.JWTUtility;
import com.naijaways.dto.Email;
import com.naijaways.entity.*;
import com.naijaways.enums.UserType;
import com.naijaways.repository.AdminRepository;
import com.naijaways.repository.BusinessUserRepository;
import com.naijaways.repository.PermissionRepository;
import com.naijaways.service.MyUserDetailsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Component
@Slf4j
@AllArgsConstructor
public class EmailTemplateSender {

    private final EmailSender emailSender;
    private final JWTUtility jwtUtility;
    private final MyUserDetailsService myUserDetailsService;
    private final PermissionRepository permissionsRepository;
    private final BusinessUserRepository businessUserRepository;
    private final AdminRepository adminRepository;

    @Async
    public void sendUserInviteMail(User user) {

        Map<String, String> props = emailProps(user);
        Email email = Email.builder()
                .subject("Invitation Email")
                .templateName("user-invitation-template")
                .to(user.getEmail())
                .templateProperties(props)
                .build();

        emailSender.send(email);
    }

    @Async
    public void sendResetPasswordMail(User user) {
        Map<String, String> props = emailProps(user);


        Email email = Email.builder()
                .subject("Reset Password")
                .templateName("reset-password-template")
                .to(user.getEmail())
                .templateProperties(props)
                .build();

    }

    private Map<String, String> emailProps(User user) {
        String firstName = "", lastName = "", token = "", businessName = "Alerzo";
        if (Objects.equals(user.getUserType(), UserType.ADMIN)) {
            Admin admin = adminRepository.findAdminByUserId(user.getId());
            List<Permission> permissions = permissionsRepository.findPermissionsByRoles(admin.getRole());
            token = jwtUtility.generateToken(new MyUserDetails(admin.getUser()
                    , myUserDetailsService.getAuthoritiesForUser(permissions)));
            firstName = admin.getFirstName();
            lastName = admin.getLastName();
        } else {
            BusinessUser businessUser = businessUserRepository.findBusinessUserByUserId(user.getId());
            List<Permission> permissions = permissionsRepository.findPermissionsByRoles(businessUser.getRole());
            token = jwtUtility.generateToken(new MyUserDetails(businessUser.getUser()
                    , myUserDetailsService.getAuthoritiesForUser(permissions)));
            businessName = businessUser.getBusiness().getName();
            firstName = businessUser.getFirstName();
            lastName = businessUser.getLastName();

        }


        String verificationLink = "https://wwww.alerzopay.com/verify?t=".concat(token);

        Map<String, String> props = new HashMap<>();
        props.put("firstName", firstName);
        props.put("lastName", lastName);
        props.put("verificationLink", verificationLink);
        props.put("token", token);
        props.put("businessName", businessName);
//        props.put("senderName", senderName);

        return props;
    }
}

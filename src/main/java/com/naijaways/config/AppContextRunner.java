package com.naijaways.config;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Configuration
@Component
@Slf4j
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class AppContextRunner implements ApplicationListener<ContextRefreshedEvent> {

        DBSeeder dbSeeder;

        @Override
        @Transactional
        public void onApplicationEvent(ContextRefreshedEvent event) {
                dbSeeder.seedDefaultRolesPermission();
        }

}

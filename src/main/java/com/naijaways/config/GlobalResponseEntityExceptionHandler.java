package com.naijaways.config;


import com.naijaways.exception.*;
import com.naijaways.utils.DataResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Optional;

@ControllerAdvice
@RestController
public class GlobalResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

        private static final Logger log = LoggerFactory.getLogger(GlobalResponseEntityExceptionHandler.class);

        @Override
        protected ResponseEntity<Object>
        handleMethodArgumentNotValid(
                MethodArgumentNotValidException ex,
                HttpHeaders headers, HttpStatus status, WebRequest request) {
                var error = DataResponseUtils.errorResponse("Validation Failed");
                error.setData(ex.getBindingResult().getFieldErrors());
                return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
        }


        @ExceptionHandler(ResourceNotFoundException.class)
        public ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException e) {
                return ResponseEntity
                        .of(Optional.of(DataResponseUtils.errorResponse(e.getMessage())));
        }


        @ExceptionHandler(Exception.class)
        public ResponseEntity<Object> handleException(Exception e) {
                log.error(e.getMessage(), e);
                return new ResponseEntity(DataResponseUtils.errorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        @ExceptionHandler(UnkwownDeviceException.class)
        public ResponseEntity<Object> handleUnknownDeviceException(UnkwownDeviceException e) {
                return ResponseEntity.badRequest().body(DataResponseUtils.errorResponse(e.getMessage()));
        }

        @ExceptionHandler(DuplicateEntityException.class)
        public ResponseEntity<Object> handleDuplicateEntityException(DuplicateEntityException e) {
                return ResponseEntity.badRequest().body(DataResponseUtils.errorResponse(e.getMessage()));
        }

        @ExceptionHandler(EntityConflictException.class)
        public ResponseEntity<Object> handleEntityConflictException(EntityConflictException e) {
                return ResponseEntity.badRequest().body(DataResponseUtils.errorResponse(e.getMessage()));
        }

        @ExceptionHandler(AuthException.class)
        public ResponseEntity<Object> handleAuthException(AuthException e) {
                return ResponseEntity.badRequest().body(DataResponseUtils.errorResponse(e.getMessage()));
        }

}

package com.naijaways.config;


import com.naijaways.dto.Email;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.StringWriter;

@Component
@Slf4j
@AllArgsConstructor
public class EmailSender {

        private final Configuration configuration;
        private final JavaMailSender javaMailSender;


        public void send(Email email) {
                try {
                        javaMailSender.send(getMime(email));
                } catch (Exception e) {
                        log.error("Unable to send mail {}", e.getMessage());
                }

        }

        public MimeMessage getMime(Email email) throws MessagingException, IOException, TemplateException {
                MimeMessage mimeMessage = javaMailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
                helper.setSubject(email.getSubject());
                helper.setTo(email.getTo());
                String emailContent = getEmailContent(email);
                helper.setText(emailContent, true);
                return mimeMessage;
        }

        String getEmailContent(Email email) throws IOException, TemplateException {
                final String TEMPLATE_FILE_EXT = ".ftl";

                StringWriter stringWriter = new StringWriter();
                configuration.setClassForTemplateLoading(this.getClass(), "/templates/email");

                configuration.getTemplate(email.getTemplateName().concat(TEMPLATE_FILE_EXT)).process(email.getTemplateProperties(), stringWriter);
                return stringWriter.getBuffer().toString();
        }
}

package com.naijaways.config.securityConfig;

import com.naijaways.service.MyUserDetailsService;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@AllArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

        private JWTUtility jwtUtility;

        private MyUserDetailsService userService;

        @Override
        protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

                String authorization = request.getHeader("Authorization");

                String token = null;
                String email = null;

                if (null != authorization && authorization.startsWith("Bearer ")) {
                        token = authorization.substring(7);
                        email = jwtUtility.getUserEmailFromToken(token);
                }

                if (null != email && SecurityContextHolder.getContext().getAuthentication() == null) {
                        UserDetails userDetails = userService.loadUserByUsername(email);

                        if (jwtUtility.validateToken(token, userDetails)) {
                                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken
                                        = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
                        }

                }
                filterChain.doFilter(request, response);
        }
}

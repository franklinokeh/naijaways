package com.naijaways.config.securityConfig;

import com.naijaways.handler.CustomAccessDeniedHandler;
import com.naijaways.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@Order(2)
public class JwtAuthSecurityConfig extends WebSecurityConfigurerAdapter {

        private static final String[] SWAGGER_WHITELIST = {
                "/v3/api-docs/**",
                "/api-docs/**",
                "/swagger-ui/**",
                "/swagger-ui.html",
        };
        private  final String[] WEB_WHITELIST = {
                "/api/v1/users/login",
                "/api/v1/users/signup",
                "/api/v1/business",
                "/api/v1/auth/send-password-reset-mail",
                "/api/v1/users/user-details"
        };
        private final MyUserDetailsService userService;
        private final PasswordEncoder passwordEncoder;
        private final JwtAuthenticationFilter jwtAuthenticationFilter;
        CustomAccessDeniedHandler accessDeniedHandler;

        @Autowired
        public JwtAuthSecurityConfig(MyUserDetailsService userService, PasswordEncoder passwordEncoder, JwtAuthenticationFilter jwtAuthenticationFilter, CustomAccessDeniedHandler accessDeniedHandler) {
                this.userService = userService;
                this.passwordEncoder = passwordEncoder;
                this.jwtAuthenticationFilter = jwtAuthenticationFilter;
                this.accessDeniedHandler = accessDeniedHandler;
        }

        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
                auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
                http
                        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                        .and()
                        .csrf().disable()
                        .antMatcher("/api/v1/**")
                        .authorizeRequests()
                        .antMatchers(WEB_WHITELIST).permitAll()
                        .antMatchers(SWAGGER_WHITELIST).permitAll()
                        .anyRequest().authenticated()
                        .and()
                        .exceptionHandling().accessDeniedHandler(accessDeniedHandler)
                        .and()
                        .addFilterBefore(
                                jwtAuthenticationFilter,
                                UsernamePasswordAuthenticationFilter.class)
                        .userDetailsService(this.userService);
        }
}

package com.naijaways.config.securityConfig;

import com.naijaways.handler.CustomAccessDeniedHandler;
import com.naijaways.service.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(1)
public class BasicAuthSecurityConfig extends WebSecurityConfigurerAdapter {

        private static final String[] SWAGGER_WHITELIST = {
                "/v3/api-docs/**",
                "/api-docs/**",
                "/api/vi/business",
                "/swagger-ui/**",
                "/swagger-ui.html",
        };
        private final MyUserDetailsService userService;
        private final PasswordEncoder passwordEncoder;
        private final CustomAccessDeniedHandler accessDeniedHandler;

        public BasicAuthSecurityConfig(MyUserDetailsService userService, PasswordEncoder passwordEncoder, CustomAccessDeniedHandler accessDeniedHandler) {
                this.userService = userService;
                this.passwordEncoder = passwordEncoder;
                this.accessDeniedHandler = accessDeniedHandler;
        }

        @Bean
        public BasicAuthenticationFilter authenticationFilter() throws Exception {
                BasicAuthenticationFilter authenticationFilter
                        = new BasicAuthenticationFilter(userService);
                authenticationFilter.setAuthenticationManager(authenticationManagerBean());
                return authenticationFilter;
        }

        @Autowired
        public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
                auth.userDetailsService(userService).passwordEncoder(passwordEncoder);
        }

        @Override
        @Bean
        public AuthenticationManager authenticationManagerBean() throws Exception {
                return super.authenticationManagerBean();
        }


        public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*")
                        .allowedMethods("GET", "POST", "PUT", "DELETE")
                        .allowedHeaders("*")
                        .exposedHeaders("Authorization")
                        .allowCredentials(true)
                        .maxAge(3600);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
                http
                        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                        .and()
                        .csrf().disable()
                        .antMatcher("/api/v1/app/**")
                        .authorizeRequests()
                        .antMatchers("/api/v1/app/users/login").permitAll()
                        .antMatchers("/api/v1/business").permitAll()
                        .antMatchers(SWAGGER_WHITELIST).permitAll()
                        .anyRequest().authenticated()
                        .and()
                        .exceptionHandling().accessDeniedHandler(accessDeniedHandler)
                        .and()
                        .addFilterBefore(
                                authenticationFilter(),
                                UsernamePasswordAuthenticationFilter.class)
                        .userDetailsService(this.userService)
                        .httpBasic();
        }
}

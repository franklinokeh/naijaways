package com.naijaways.config.securityConfig;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.naijaways.models.LoginRequestDto;
import com.naijaways.service.MyUserDetailsService;
import lombok.AllArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@AllArgsConstructor
public class BasicAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

        private static final Log LOG = LogFactory.getLog(BasicAuthenticationFilter.class);
        private static final String ERROR_MESSAGE = "Something went wrong while parsing /login request body";
        private final ObjectMapper objectMapper = new ObjectMapper();
        private final MyUserDetailsService myUserDetailsService;

        @Override
        public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
                String requestBody;
                try {
                        requestBody = IOUtils.toString(request.getReader());
                        LoginRequestDto authRequest = objectMapper.readValue(requestBody, LoginRequestDto.class);
                        UserDetails userDetails = myUserDetailsService.loadUserByUsername(authRequest.getEmail());
                        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                        setDetails(request, token);
                        SecurityContextHolder.getContext().setAuthentication(token);
                        return this.getAuthenticationManager().authenticate(token);
                } catch (IOException e) {
                        LOG.error(ERROR_MESSAGE, e);
                        throw new InternalAuthenticationServiceException(ERROR_MESSAGE, e);
                }

        }
}

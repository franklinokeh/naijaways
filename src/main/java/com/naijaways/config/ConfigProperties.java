package com.naijaways.config;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

@Data
@ConfigurationProperties(prefix = "naijaways")
@FieldDefaults(level = AccessLevel.PRIVATE)
@ComponentScan(basePackages = {"com.naijaways.config"})
public class ConfigProperties {

}

package com.naijaways.config;

public class NaijaWaysConstants {

        public static final String SUPER_ADMIN_DEFAULT_ROLE = "ROOT";
        public static final String SUPER_ADMIN_DEFAULT_EMAIL = "frankok101@gmail.com";
        public static final String SUPER_ADMIN_DEFAULT_PASSWORD = "password";
        public static final String BUSINESS_ADMIN_DEFAULT_ROLE = "BUSINESS";
        public static final String PASSWORD_PATTERN = "^(?=\\D*\\d)\\S{6,}$";

        public static final String BUSINESS_MARKETER = "MARKETER";
        public static final String BUSINESS_TRANSPORT_MANAGER = "TRANSPORT MANAGER";
        public static final String BUSINESS_STORE_MANAGER = "STORE MANAGER";
        public static final String BUSINESS_OTHER = "OTHER";
        public static final String BUSINESS_ADMIN = "ADMIN";
        public static final String BUSINESS_REGIONAL_SALES_PERSONNEL = "REGIONAL SALES PERSONNEL";
}

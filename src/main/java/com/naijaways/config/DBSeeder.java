package com.naijaways.config;


import com.naijaways.entity.Admin;
import com.naijaways.entity.Permission;
import com.naijaways.entity.Role;
import com.naijaways.entity.User;
import com.naijaways.enums.*;
import com.naijaways.repository.PermissionRepository;
import com.naijaways.repository.RoleRepository;
import com.naijaways.repository.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static com.naijaways.config.NaijaWaysConstants.SUPER_ADMIN_DEFAULT_EMAIL;
import static com.naijaways.config.NaijaWaysConstants.SUPER_ADMIN_DEFAULT_PASSWORD;


@Configuration
@Slf4j
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class DBSeeder {
        UserRepository userRepo;
        RoleRepository roleRepo;
        PermissionRepository permissionRepo;
        PasswordEncoder passwordEncoder;


        @Transactional
        public void seedDefaultRolesPermission() {

                Permission addTransaction = createOrUpdatePermission(PermissionName.TRANSACTION_ADD, PermissionCategory.TRANSACTION, PermissionType.GLOBAL);
                Permission viewTransaction = createOrUpdatePermission(PermissionName.TRANSACTION_VIEW, PermissionCategory.TRANSACTION, PermissionType.GLOBAL);
                Permission downloadTransaction = createOrUpdatePermission(PermissionName.TRANSACTION_DOWNLOAD, PermissionCategory.TRANSACTION, PermissionType.GLOBAL);
                Permission reprocessTransaction = createOrUpdatePermission(PermissionName.TRANSACTION_REPROCESS, PermissionCategory.TRANSACTION, PermissionType.GLOBAL);
                Permission adminAddTransaction = createOrUpdatePermission(PermissionName.ADMIN_TRANSACTION_ADD, PermissionCategory.TRANSACTION, PermissionType.ADMIN);
                Permission adminViewTransaction = createOrUpdatePermission(PermissionName.ADMIN_TRANSACTION_VIEW, PermissionCategory.TRANSACTION, PermissionType.ADMIN);
                Permission adminDownloadTransaction = createOrUpdatePermission(PermissionName.ADMIN_TRANSACTION_DOWNLOAD, PermissionCategory.TRANSACTION, PermissionType.ADMIN);
                Permission adminReprocessTransaction = createOrUpdatePermission(PermissionName.ADMIN_TRANSACTION_REPROCESS, PermissionCategory.TRANSACTION, PermissionType.ADMIN);
                Permission switchProviders = createOrUpdatePermission(PermissionName.CONFIGURATION_SWITCH_PROVIDERS, PermissionCategory.CONFIGURATION, PermissionType.ADMIN);
                Permission binRouting = createOrUpdatePermission(PermissionName.CONFIGURATION_BIN_ROUTING, PermissionCategory.CONFIGURATION, PermissionType.ADMIN);
                Permission setAmount = createOrUpdatePermission(PermissionName.CONFIGURATION_AMOUNT, PermissionCategory.CONFIGURATION, PermissionType.ADMIN);
                Permission addBusiness = createOrUpdatePermission(PermissionName.BUSINESS_ADD, PermissionCategory.BUSINESS, PermissionType.ADMIN);
                Permission viewBusiness = createOrUpdatePermission(PermissionName.BUSINESS_VIEW, PermissionCategory.BUSINESS, PermissionType.ADMIN);
                Permission deleteBusiness = createOrUpdatePermission(PermissionName.BUSINESS_DELETE, PermissionCategory.BUSINESS, PermissionType.ADMIN);
                Permission editBusiness = createOrUpdatePermission(PermissionName.BUSINESS_EDIT, PermissionCategory.BUSINESS, PermissionType.ADMIN);
                Permission toggleBusiness = createOrUpdatePermission(PermissionName.BUSINESS_TOGGLE, PermissionCategory.BUSINESS, PermissionType.ADMIN);
                Permission resetPassword = createOrUpdatePermission(PermissionName.SETTINGS_RESET_PASSWORD, PermissionCategory.SETTINGS, PermissionType.GLOBAL);
                Permission adminResetPassword = createOrUpdatePermission(PermissionName.ADMIN_SETTINGS_RESET_PASSWORD, PermissionCategory.SETTINGS, PermissionType.ADMIN);
                Permission removeMember = createOrUpdatePermission(PermissionName.PERMISSION_REMOVE_MEMBER, PermissionCategory.SETTINGS, PermissionType.GLOBAL);
                Permission changeRole = createOrUpdatePermission(PermissionName.PERMISSION_CHANGE_ROLE, PermissionCategory.SETTINGS, PermissionType.GLOBAL);
                Permission createRole = createOrUpdatePermission(PermissionName.PERMISSION_CREATE_ROLE, PermissionCategory.SETTINGS, PermissionType.GLOBAL);
                Permission editRole = createOrUpdatePermission(PermissionName.PERMISSION_EDIT_ROLE, PermissionCategory.SETTINGS, PermissionType.GLOBAL);
                Permission inviteMember = createOrUpdatePermission(PermissionName.PERMISSION_INVITE_MEMBER, PermissionCategory.SETTINGS, PermissionType.GLOBAL);
                Permission toggleMember = createOrUpdatePermission(PermissionName.PERMISSION_TOGGLE_MEMBER, PermissionCategory.SETTINGS, PermissionType.GLOBAL);
                Permission adminRemoveMember = createOrUpdatePermission(PermissionName.ADMIN_PERMISSION_REMOVE_MEMBER, PermissionCategory.SETTINGS, PermissionType.ADMIN);
                Permission adminChangeRole = createOrUpdatePermission(PermissionName.ADMIN_PERMISSION_CHANGE_ROLE, PermissionCategory.SETTINGS, PermissionType.ADMIN);
                Permission adminCreateRole = createOrUpdatePermission(PermissionName.ADMIN_PERMISSION_CREATE_ROLE, PermissionCategory.SETTINGS, PermissionType.ADMIN);
                Permission adminEditRole = createOrUpdatePermission(PermissionName.ADMIN_PERMISSION_EDIT_ROLE, PermissionCategory.SETTINGS, PermissionType.ADMIN);
                Permission adminInviteMember = createOrUpdatePermission(PermissionName.ADMIN_PERMISSION_INVITE_MEMBER, PermissionCategory.SETTINGS, PermissionType.ADMIN);
                Permission adminToggleMember = createOrUpdatePermission(PermissionName.ADMIN_PERMISSION_TOGGLE_MEMBER, PermissionCategory.SETTINGS, PermissionType.ADMIN);


                List<Permission> superAdminPermissions = Arrays.asList(adminRemoveMember, adminChangeRole, adminCreateRole, adminEditRole,
                        adminInviteMember, adminToggleMember, adminResetPassword, adminAddTransaction, adminViewTransaction,
                        adminDownloadTransaction, adminReprocessTransaction, addBusiness, viewBusiness, deleteBusiness,
                        editBusiness, toggleBusiness, switchProviders, binRouting, setAmount);

                List<Permission> businessAdminPermissions = Arrays.asList(removeMember, changeRole, createRole, editRole,
                        inviteMember, toggleMember, resetPassword, addTransaction, viewTransaction,
                        downloadTransaction, reprocessTransaction);

                Role SUPER_ADMIN_ROLE = createOrUpdateRole(NaijaWaysConstants.SUPER_ADMIN_DEFAULT_ROLE, RoleType.ADMIN,
                        superAdminPermissions);

                Role DEFAULT_BUSINESS_ADMIN_ROLE = createOrUpdateRole(NaijaWaysConstants.BUSINESS_ADMIN_DEFAULT_ROLE, RoleType.BUSINESS, businessAdminPermissions);
                Role MARKETER = createOrUpdateRole(NaijaWaysConstants.BUSINESS_MARKETER, RoleType.BUSINESS,
                        businessAdminPermissions);
                Role TRANSPORT_MANAGER = createOrUpdateRole(NaijaWaysConstants.BUSINESS_TRANSPORT_MANAGER,
                        RoleType.BUSINESS, businessAdminPermissions);
                Role STORE_MANAGER = createOrUpdateRole(NaijaWaysConstants.BUSINESS_STORE_MANAGER, RoleType.BUSINESS,
                        businessAdminPermissions);
                Role OTHER = createOrUpdateRole(NaijaWaysConstants.BUSINESS_OTHER, RoleType.BUSINESS,
                        businessAdminPermissions);
                Role ADMIN = createOrUpdateRole(NaijaWaysConstants.BUSINESS_ADMIN, RoleType.BUSINESS,
                        businessAdminPermissions);
                Role REGIONAL_SALES_PERSONNEL = createOrUpdateRole(NaijaWaysConstants.BUSINESS_REGIONAL_SALES_PERSONNEL,
                        RoleType.BUSINESS, businessAdminPermissions);



                if (userRepo.existsByEmail(SUPER_ADMIN_DEFAULT_EMAIL)) {
                        log.info("==================> Existing user <===============");
                        return;
                }
                User user = new User();
                user.setUserType(UserType.ADMIN);
                user.setPassword(passwordEncoder.encode(SUPER_ADMIN_DEFAULT_PASSWORD));
                user.setEmail(SUPER_ADMIN_DEFAULT_EMAIL);
                user.setUsername(SUPER_ADMIN_DEFAULT_EMAIL);
                user.setPhoneNumber("08149959190");
                Admin adminUser = new Admin();
                adminUser.setFirstName("Franklin");
                adminUser.setLastName("Okosisi");
                adminUser.setRole(SUPER_ADMIN_ROLE);
                user.setAdminUser(adminUser);
                adminUser.setUser(user);
                user.setAccountVerified(true);
                user.setAccountNonLocked(true);
                userRepo.save(user);
        }


        private Permission createOrUpdatePermission(PermissionName name, PermissionCategory category, PermissionType type) {
                return permissionRepo.findByName(name).map(perm -> {
                        perm.setPermissionType(type);
                        perm.setCategory(category);
                        return permissionRepo.save(perm);
                }).orElseGet(() -> {
                        Permission permission = new Permission();
                        permission.setName(name);
                        permission.setPermissionType(type);
                        permission.setCategory(category);
                        return permissionRepo.save(permission);
                });
        }

        @Transactional
        Role createOrUpdateRole(String name, RoleType type, Collection<Permission> permissions) {
                return roleRepo.findByNameAndRoleType(name, type).map(role -> {
                        var permissionSet = permissions.stream().map(Permission::getName).collect(Collectors.toSet());
                        var rolePermission = role.getPermissions().stream().map(Permission::getName).collect(Collectors.toSet());
                        role.getPermissions().removeIf(permission -> BooleanUtils.isFalse(permissionSet.contains(permission.getName())));
                        for (var permission : permissions) {
                                if (Boolean.FALSE.equals(rolePermission.contains(permission.getName()))) {
                                        role.getPermissions().add(permission);
                                        permission.getRoles().add(role);
                                }
                        }
                        permissionRepo.saveAll(permissions);
                        return roleRepo.save(role);
                }).orElseGet(() -> {
                        Role role = new Role();
                        role.setRoleType(type);
                        role.setName(name);
                        role.setPermissions(new ArrayList<>(permissions));
                        roleRepo.save(role);
                        permissions.forEach(perm -> perm.getRoles().add(role));
                        return role;
                });
        }

}

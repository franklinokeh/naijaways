package com.naijaways.audit;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.naijaways.config.DateTimeSerialize;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<U> {

        @CreatedBy
        @Column(name = "created_by")
        protected U createdBy;

        @CreationTimestamp
        @JsonSerialize(using = DateTimeSerialize.class)
        @Column(name = "created_date", nullable = false)
        protected LocalDateTime createdDate;

        @LastModifiedBy
        @Column(name = "last_modified_by")
        protected U lastModifiedBy;

        @UpdateTimestamp
        @JsonSerialize(using = DateTimeSerialize.class)
        @Column(name = " last_modified_date", nullable = false)
        protected LocalDateTime lastModifiedDate;

}

package com.naijaways.entity;


import com.naijaways.audit.Auditable;
import com.naijaways.enums.UserType;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class User extends Auditable<String> {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        private Long id;

        @Column(name = "email", unique = true, nullable = false)
        private String email;

        @Column(name = "username", unique = true, nullable = false)
        private String username;

        @Column(name = "password", nullable = false)
        private String password;

        @Column(name = "phone_number", nullable = true)
        private String phoneNumber;

        @Column(name = "firebase_token")
        private String firebaseToken;

        @Column(name = "enabled", nullable = false)
        private boolean isAccountVerified;

        @Column(name = "non_locked", nullable = false)
        private boolean isAccountNonLocked;

        @Enumerated(EnumType.STRING)
        private UserType userType;

        @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true, mappedBy = "user")
        private Admin adminUser;

        @OneToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST}, orphanRemoval = true, mappedBy = "user")
        private BusinessUser businessUser;

}

package com.naijaways.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.audit.Auditable;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "payments")
public class Payment extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        private Long id;

        @ManyToOne
        @JsonIgnore
        @JoinColumn(name = "business_id", nullable = false)
        private Business business;
}

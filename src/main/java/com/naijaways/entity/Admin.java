package com.naijaways.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.audit.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Getter
@Setter
@Table(name = "admins")
public class Admin extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        private Long id;

        @Column(name = "first_name", nullable = false)
        private String firstName;

        @Column(name = "last_name", nullable = false)
        private String lastName;

        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "user_id", nullable = false)
        private User user;

        @ManyToOne
        @JsonIgnore
        @JoinColumn(name = "role_id", nullable = false)
        private Role role;
}

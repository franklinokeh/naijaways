package com.naijaways.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.audit.Auditable;
import com.naijaways.enums.OrderStatus;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
public class Order extends Auditable<String> {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        private Long id;

        @OneToMany
        @JoinColumn(name = "shipment_id")
        private List<Shipment> shipments = new ArrayList<>();


        @ManyToOne
        @JoinColumn(name = "vehicle_id")
        private Vehicle vehicle;

        @Enumerated(EnumType.STRING)
        private OrderStatus status;

        @ManyToOne(cascade = CascadeType.ALL)
        @JsonIgnore
        @JoinColumn(name = "business_id")
        private Business business;

        @ManyToOne(cascade = CascadeType.ALL)
        @JsonIgnore
        @JoinColumn(name = "business_user_id")
        private BusinessUser businessUser;

}

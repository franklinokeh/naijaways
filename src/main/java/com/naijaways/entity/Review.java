package com.naijaways.entity;

import com.naijaways.audit.Auditable;

import javax.persistence.*;

@Entity
@Table(name = "reviews")
public class Review  extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @ManyToOne
        @JoinColumn(name = "business_id")
        private Business business;

        @Column(name = "rating")
        private Integer rating;

        @Column(name = "comment")
        private String comment;

}

package com.naijaways.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.audit.Auditable;
import com.naijaways.enums.RoleType;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "roles")
public class Role extends Auditable<String> {
        @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
        @JsonIgnore
        @JoinTable(
                name = "roles_permissions",
                joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
        List<Permission> permissions = new ArrayList<>();
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        private Long id;
        @Column(name = "name", nullable = false)
        private String name;
        @Enumerated(EnumType.STRING)
        private RoleType roleType;
        @ManyToOne
        @JoinColumn(name = "business_id", referencedColumnName = "id")
        private Business business;

}

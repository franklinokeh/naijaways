package com.naijaways.entity;

import com.naijaways.audit.Auditable;

import javax.persistence.*;

@Entity
@Table(name = "routes")
public class Route  extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "start_location")
        private String startLocation;

        @Column(name = "end_location")
        private String endLocation;

        @Column(name = "intermediate_stops")
        private String intermediateStops;

}

package com.naijaways.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.audit.Auditable;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "business")
public class Business extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "business_name", unique = true, nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "logo")
    private String logo;

    @Column(name = "kyc_status", nullable = false)
    private boolean kycStatus;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "enabled", nullable = false)
    private boolean isBusinessEnabled;

    @OneToMany(mappedBy = "business", orphanRemoval = true)
    private List<Address> addresses = new ArrayList<>();

    @OneToMany(mappedBy = "business", orphanRemoval = true)
    private List<Payment> payments = new ArrayList<>();

    @OneToMany(mappedBy = "business", orphanRemoval = true)
    private List<Order> orders = new ArrayList<>();

    @OneToMany(mappedBy = "business", orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Role> roles = new ArrayList<>();



}

package com.naijaways.entity;


import com.naijaways.audit.Auditable;
import com.naijaways.enums.PermissionCategory;
import com.naijaways.enums.PermissionName;
import com.naijaways.enums.PermissionType;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "permissions")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Permission extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        Long id;

        @Enumerated(EnumType.STRING)
        PermissionName name;

        @Enumerated(EnumType.STRING)
        PermissionType permissionType;

        @Enumerated(EnumType.STRING)
        PermissionCategory category;


        @ManyToMany(mappedBy = "permissions")
        List<Role> roles = new ArrayList<>();
}

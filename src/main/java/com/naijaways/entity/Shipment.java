package com.naijaways.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.audit.Auditable;
import com.naijaways.enums.ShipmentStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
@Table(name = "shipments")
public class Shipment  extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "sender")
        private String sender;

        @Column(name = "sender_phone")
        private String senderPhone;

        @Column(name = "origin")
        private String origin;

        @Column(name = "destination")
        private String destination;

        @Column(name = "weight")
        private BigDecimal weight;

        @Column(name = "dimensions")
        private String dimensions;

        @Column(name = "status")
        @Enumerated(EnumType.STRING)
        private ShipmentStatus status;

        @Column(name = "trackin_no")
        private String trackingNo;

        @Column(name = "recipient")
        private String recipient;

        @Column(name = "recipient_phone")
        private String recipientPhone;

        @Column(name = "delivery_note")
        private String deliveryNote;

        @Column(name = "method_of_shipment")
        private String methodOfShipment;

        @Column(name = "cost_of_shipment")
        private BigDecimal costOfShipment;

        @Column(name = "receipt_note")
        private String receiptNote;

        @ManyToOne
        private Order order;

        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "payment_id", nullable = false)
        private Payment paymentInfo;

        @ManyToOne(cascade = CascadeType.ALL)
        //@JsonIgnore
        @JoinColumn(name = "business_id")
        private Business business;

        @ManyToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "business_user_id")
        private BusinessUser businessUser;

}

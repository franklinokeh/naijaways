package com.naijaways.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.audit.Auditable;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "business_users")
public class BusinessUser extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        private Long id;

        @Column(name = "first_name", nullable = false)
        private String firstName;

        @Column(name = "last_name", nullable = false)
        private String lastName;

        @ManyToOne
        @JsonIgnore
        @JoinColumn(name = "business_id", nullable = false)
        private Business business;

        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "user_id", nullable = false)
        private User user;

        @ManyToOne
        @JsonIgnore
        @JoinColumn(name = "role_id", nullable = false)
        private Role role;

        @Column(name = "current_location")
        private String currentLocation;
}

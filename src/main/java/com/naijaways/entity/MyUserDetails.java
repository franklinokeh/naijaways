package com.naijaways.entity;

import com.naijaways.enums.UserType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

public class MyUserDetails implements UserDetails {

        private final User user;
        private final Collection<? extends GrantedAuthority> authorities;

        public MyUserDetails(User user, Collection<? extends GrantedAuthority> authorities) {
                this.user = user;
                this.authorities = authorities;
        }

        public MyUserDetails(User user) {
                this(user, getAuthority(user));
        }

        static Collection<GrantedAuthority> getAuthority(User user) {
                Role role = user.getUserType().equals(UserType.ADMIN) ? user.getAdminUser().getRole() : user.getBusinessUser().getRole();

                return role.getPermissions().stream()
                        .map(permission -> new SimpleGrantedAuthority(permission.getName().name()))
                        .collect(Collectors.toList());
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
                return authorities;
        }

        @Override
        public String getPassword() {
                return user.getPassword();
        }

        @Override
        public String getUsername() {
                return user.getEmail();
        }

        @Override
        public boolean isAccountNonExpired() {
                return true;
        }

        @Override
        public boolean isAccountNonLocked() {
                return user.isAccountNonLocked();
        }

        @Override
        public boolean isCredentialsNonExpired() {
                return true;
        }

        @Override
        public boolean isEnabled() {
                return user.isAccountVerified();
        }
}

package com.naijaways.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.audit.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "vehicles")
public class Vehicle  extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @Column(name = "make")
        private String make;

        @Column(name = "model")
        private String model;

        @Column(name = "capacity")
        private Integer capacity;

        @Column(name = "current_location")
        private String currentLocation;

        @Column(name = "current_latitude")
        private double currentLatitude;

        @Column(name = "current_longitude")
        private double currentLongitude;

        @Column(name = "current_fuel_level")
        private double currentFuelLevel;

        @Column(name = "is_available")
        private boolean isAvailable;

        @ManyToOne
        @JsonIgnore
        @JoinColumn(name = "business_id", nullable = false)
        private Business business;

        @OneToOne
        @JsonIgnore
        @JoinColumn(name = "business_user_id", nullable = false)
        private BusinessUser businessUser;


}

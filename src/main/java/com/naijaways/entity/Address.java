package com.naijaways.entity;

import com.naijaways.audit.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "address")
public class Address extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        private Long id;

        @Column(name = "street_address")
        private String streetAddress;

        @Column(name = "city")
        private String city;

        @Column(name = "state")
        private String state;

        @Column(name = "zip_code")
        private String zipCode;


        @ManyToOne(cascade = CascadeType.ALL, optional = false)
        @JoinColumn(name = "business_id", nullable = false, unique = true)
        private Business business;

}

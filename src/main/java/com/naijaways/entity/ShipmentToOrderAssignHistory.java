package com.naijaways.entity;

import com.naijaways.audit.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "shipment_to_order_assign_history")
public class ShipmentToOrderAssignHistory extends Auditable<String> {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id", nullable = false)
        private Long id;


        @ManyToOne
        @JoinColumn(name = "order_id")
        private Order Order;

        @ManyToOne
        @JoinColumn(name = "shipment_id")
        private Shipment shipment;

        @ManyToOne
        @JoinColumn(name = "business_user_id")
        private BusinessUser businessUser;

}

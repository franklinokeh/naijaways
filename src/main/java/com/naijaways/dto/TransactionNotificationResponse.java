package com.naijaways.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionNotificationResponse {
        private boolean status;
        private String reference;
}

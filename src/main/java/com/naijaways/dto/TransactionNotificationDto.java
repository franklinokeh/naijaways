package com.naijaways.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionNotificationDto {
        private String amount;
        private String terminalId;
        private String statusCode;
        private String pan;
        private String rrn;
        private String bank;
        private boolean reversed;
        private String stan;
        private String transactionTime;
        private String processor;
        private long businessId;
        private String transactionRefNo;
}

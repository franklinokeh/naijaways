package com.naijaways.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ShipmentsSummary {
        private String totalShipment;
        private String deliveredShipment;
        private String pendingShipment;
        private String canceledShipment;
}

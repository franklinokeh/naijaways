package com.naijaways.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@Builder
public class Email implements Serializable {
        private static final long serialVersionUID = 6481794982612826257L;
        private String from;
        private String fromEmail;
        private String to;
        private String subject;
        private String templateName;
        private Map<String, String> templateProperties;

}

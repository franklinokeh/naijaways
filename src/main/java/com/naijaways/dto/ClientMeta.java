package com.naijaways.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientMeta {
        private String clientId;
        private String clientUrl;
        private String clientToken;
        private String authType;
}

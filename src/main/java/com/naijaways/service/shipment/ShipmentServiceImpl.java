package com.naijaways.service.shipment;

import com.naijaways.dto.ShipmentsSummary;
import com.naijaways.entity.*;
import com.naijaways.enums.ShipmentStatus;
import com.naijaways.exception.BadRequestException;
import com.naijaways.models.*;
import com.naijaways.repository.OrderRepository;
import com.naijaways.repository.ShipmentRepository;
import com.naijaways.repository.ShipmentToOrderAssignHistoryRepository;
import com.naijaways.service.user.UserService;
import com.naijaways.utils.Misc;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class ShipmentServiceImpl implements  ShipmentService{
        private final ShipmentToOrderAssignHistoryRepository shipmentToOrderAssignHistoryRepository;
        private final OrderRepository orderRepository;
        private final ShipmentRepository shipmentRepository;
        private final UserService userService;

        @Override
        public ResponseEntity<ApiDataResponseDto> createShipments(CreateOrderDto createOrderDto) {
                User user = userService.getLoggedInUser();
                BusinessUser businessUser = user.getBusinessUser();
                Business business = businessUser.getBusiness();
                String trackingNo = Misc.generateRef();
                ///////////////////////////////////////////////////
                Shipment newShipment = new Shipment();
                newShipment.setCostOfShipment(new BigDecimal(createOrderDto.getCostOfShipment()));
                newShipment.setMethodOfShipment(createOrderDto.getMethodOfShipment());
                newShipment.setOrigin(createOrderDto.getOrigin());
                newShipment.setStatus(ShipmentStatus.CREATED);
                newShipment.setDestination(createOrderDto.getDestination());
                newShipment.setDimensions(createOrderDto.getDimension());
                newShipment.setSender(createOrderDto.getSender());
                newShipment.setRecipient(createOrderDto.getRecipient());
                newShipment.setWeight(createOrderDto.getWeight());
                newShipment.setDeliveryNote(createOrderDto.getDeliveryNote());
                newShipment.setReceiptNote(null);
                newShipment.setRecipientPhone(createOrderDto.getRecipientPhone());
                newShipment.setTrackingNo(trackingNo);
                newShipment.setBusiness(business);
                newShipment.setBusinessUser(businessUser);
                try {
                        shipmentRepository.save(newShipment);
                        HashMap<String, String> response = new HashMap<>();
                        response.put("trackingNo", trackingNo);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, "Shipment " +
                                "created successfully", response.toString(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
                }catch (BadRequestException ex){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, "" +
                                "Error in creating new order","", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.BAD_REQUEST);
                }

        }

        @Override
        public ResponseEntity<ApiDataResponseDto> getShipments(SortOrdersDto pageable) {
                Pageable paging = PageRequest.of(pageable.getPaging().getPage(), pageable.getPaging().getPageSize());
                Page<Shipment> shipmentPage = shipmentRepository.findAll(paging);
                if (shipmentPage.isEmpty()) {
                        ApiDataResponseDto apiDataResponseDto =  new ApiDataResponseDto(true, 200,
                                shipmentPage, "Successfully " +
                                "retrieved shipments", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
                }
                ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, shipmentPage, "Shipments " +
                        "were not found", LocalDateTime.now().toString());
                return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> getShipmentSummary(ShipmentSummaryDto shipmentSummary) {

                ShipmentsSummary shipmentsSummary = ShipmentsSummary.builder()
                        .totalShipment("20,500")
                        .canceledShipment("500")
                        .pendingShipment("2,000")
                        .deliveredShipment("18,000")
                        .build();
                ApiDataResponseDto apiDataResponseDto =  new ApiDataResponseDto(true, 200,
                        shipmentsSummary, "Shipments summary", LocalDateTime.now().toString());
                return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> updateShipment(UpdateShipmentDto updateShipmentDto) {
                Shipment shipment =
                        shipmentRepository.getShipmentByTrackingNo(updateShipmentDto.getTrackingNo());
                if(ObjectUtils.isEmpty(shipment))
                        throw new EntityNotFoundException("Shipment does not exist");
                shipment.setStatus(ShipmentStatus.valueOf(updateShipmentDto.getStatus()));
                shipmentRepository.save(shipment);
                try{
                        shipmentRepository.save(shipment);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, "", "Successfully updated shipment", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
                }catch (Exception ex){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, "", "Error updating shipment ", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
                }
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> assignShipmentToOrder(AssignShipmentDto assignShipmentDto) {
                Shipment shipment =
                        shipmentRepository.getShipmentByTrackingNo(assignShipmentDto.getShipmentTrackingNo());
                if(ObjectUtils.isEmpty(shipment))
                        throw new EntityNotFoundException("Shipment does not exist");

                Order order = orderRepository.findById(Long.valueOf(assignShipmentDto.getOrderId()))
                        .orElseThrow(() -> new EntityNotFoundException("Order does not exist"));

                User user = userService.getLoggedInUser();
                BusinessUser businessUser = user.getBusinessUser();

                ShipmentToOrderAssignHistory newAssign = new ShipmentToOrderAssignHistory();
                newAssign.setShipment(shipment);
                newAssign.setBusinessUser(businessUser);
                newAssign.setOrder(order);
                List<Shipment> shipmentList = order.getShipments();
                shipmentList.add(shipment);
                order.setShipments(shipmentList);
                orderRepository.save(order);
                try{
                        shipmentToOrderAssignHistoryRepository.save(newAssign);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, "", "Assigned " +
                                "shipment("+assignShipmentDto.getShipmentTrackingNo()+")", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
                }catch (Exception ex){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, "", "Error " +
                                "Assigning shipment("+assignShipmentDto.getShipmentTrackingNo()+")",
                                LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
                }
        }
}

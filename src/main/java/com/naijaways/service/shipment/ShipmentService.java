package com.naijaways.service.shipment;

import com.naijaways.models.*;
import org.springframework.http.ResponseEntity;

public interface ShipmentService {
        ResponseEntity<ApiDataResponseDto> createShipments(CreateOrderDto createOrderDto);

        ResponseEntity<ApiDataResponseDto> getShipments(SortOrdersDto pageable);

        ResponseEntity<ApiDataResponseDto> getShipmentSummary(ShipmentSummaryDto shipmentSummary);

        ResponseEntity<ApiDataResponseDto> updateShipment(UpdateShipmentDto updateShipmentDto);

        ResponseEntity<ApiDataResponseDto> assignShipmentToOrder(AssignShipmentDto assignShipmentDto);
}

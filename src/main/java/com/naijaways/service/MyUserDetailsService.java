package com.naijaways.service;

import com.naijaways.entity.*;
import com.naijaways.enums.UserType;
import com.naijaways.repository.AdminRepository;
import com.naijaways.repository.BusinessUserRepository;
import com.naijaways.repository.UserRepository;
import com.naijaways.service.permission.PermissionService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@AllArgsConstructor
@Service
public class MyUserDetailsService implements UserDetailsService {

         private final UserRepository userRepository;
         private final AdminRepository adminRepository;
         private  final BusinessUserRepository businessUserRepository;
         private final PermissionService permissionService;


        @Override
        public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
                User user = userRepository.findUserByEmail(email);
                if (user == null) {
                        throw new UsernameNotFoundException("User not found : " + email);
                }
                if (user.getUserType().equals(UserType.ADMIN) && user.isAccountVerified()){
                        Admin admin = adminRepository.findAdminByUserId(user.getId());
                        List<Permission> adminPermissions = permissionService.getPermissionsByRoles(admin.getRole());
                        return new MyUserDetails(user, getAuthoritiesForUser(adminPermissions));
                }
                if (user.getUserType().equals(UserType.BUSINESS) && user.isAccountVerified()) {
                        BusinessUser businessUser = businessUserRepository.findBusinessUserByUserId(user.getId());
                        List<Permission> businessPermissions = permissionService.getPermissionsByRoles(businessUser.getRole());
                        return new MyUserDetails(user, getAuthoritiesForUser(businessPermissions));
                }
                throw new RuntimeException("Account is disabled");
        }

        public Collection<? extends GrantedAuthority> getAuthoritiesForUser(List<Permission> adminPermissions) {
                List<GrantedAuthority> authorities = new ArrayList<>();
                for (Permission permission : adminPermissions) {
                        authorities.add(new SimpleGrantedAuthority(permission.getName().name()));
                }
                return authorities;
        }
}

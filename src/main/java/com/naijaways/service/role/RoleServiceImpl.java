package com.naijaways.service.role;


import com.naijaways.config.NaijaWaysConstants;
import com.naijaways.entity.Business;
import com.naijaways.entity.Permission;
import com.naijaways.entity.Role;
import com.naijaways.enums.RoleType;
import com.naijaways.exception.ResourceNotFoundException;
import com.naijaways.exception.RoleAlreadyExistException;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.CreateRoleDto;
import com.naijaways.models.RoleDto;
import com.naijaways.repository.AdminRepository;
import com.naijaways.repository.BusinessRepository;
import com.naijaways.repository.PermissionRepository;
import com.naijaways.repository.RoleRepository;
import com.naijaways.service.permission.PermissionService;
import com.naijaways.utils.DataResponseUtils;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Slf4j
public class RoleServiceImpl implements RoleService {

        RoleRepository roleRepository;
        PermissionRepository permissionsRepository;
        BusinessRepository businessRepository;
        AdminRepository adminRepository;
        PermissionService permissionService;

        @Override
        public ApiDataResponseDto getBusinessRole(Long businessId) {
                List<RoleDto> data = roleRepository.findAllByBusinessId(businessId).stream()
                        .map(role -> new RoleDto(role.getRoleType().name(), role.getName(), role.getId()))
                        .collect(Collectors.toList());
                return DataResponseUtils.successResponse("Retrieved business roles", data);
        }

        @Override
        public ApiDataResponseDto getAdminRoles() {
                List<RoleDto> data = roleRepository.findAllByRoleType(RoleType.ADMIN).stream()
                        .map(role -> new RoleDto(role.getRoleType().name(), role.getName(), role.getId()))
                        .collect(Collectors.toList());
                return DataResponseUtils.successResponse("Retrieved admin roles", data);
        }

        @Override
        public Role createAdminRole(CreateRoleDto createRoleDto) {
                Role role = new Role();
                List<Permission> permissions = new ArrayList<>();
                if (createRoleDto.getRoleName().equals(NaijaWaysConstants.SUPER_ADMIN_DEFAULT_ROLE)) {
                        throw new RuntimeException("Role name cannot be used");
                }

                Optional<Role> roleRetrieved = roleRepository.findByNameAndRoleType(createRoleDto.getRoleName(), RoleType.ADMIN);
                if (roleRetrieved.isPresent()) {
                        throw new RoleAlreadyExistException("Role name already exist");
                }
//                for (Long permissionId : createRoleDto.getPermissionsId()) {
//                        try {
//                                Optional<Permission> permission = permissionsRepository.findById(permissionId);
//                                permission.ifPresent(permissions::add);
//                        } catch (Exception e) {
//
//                        }
//                }
                role.setName(createRoleDto.getRoleName());
                role.setRoleType(RoleType.ADMIN);
                role.setPermissions(permissions);
                return roleRepository.save(role);
        }

        @Override
        public Role createBusinessRole(Long businessId, CreateRoleDto createRoleDto) {
                Role role = new Role();
                List<Permission> permissions = new ArrayList<>();

                Optional<Business> business = businessRepository.findById(businessId);
                if (business.isEmpty()) {
                        throw new ResourceNotFoundException("Business does not exist");
                }

                if (createRoleDto.getRoleName().equals(NaijaWaysConstants.BUSINESS_ADMIN_DEFAULT_ROLE)) {
                        throw new RuntimeException("Role name cannot be used");
                }

                Optional<Role> roleRetrieved = roleRepository.findByNameAndRoleTypeAndBusinessId(createRoleDto.getRoleName(),
                        RoleType.BUSINESS, business.get().getId());
                if (roleRetrieved.isPresent()) {
                        throw new RoleAlreadyExistException("Role name already exist");
                }

//                for (Long permissionId : createRoleDto.getPermissionsId()) {
//
//                        try {
//                                Optional<Permission> permission  = permissionsRepository.findById(permissionId);
//                                permission.ifPresent(permissions::add);
//                        } catch (Exception e) {
//
//                        }
//                }

                role.setName(createRoleDto.getRoleName());
                role.setRoleType(RoleType.BUSINESS);
                role.setBusiness(business.get());
                role.setPermissions(permissions);

                return roleRepository.save(role);
        }

        @Override
        public ApiDataResponseDto addPermissionToRole(List<Long> permId, Long roleId) {
                Role role = roleRepository.findById(roleId).orElseThrow();
                List<Permission> permissions = permissionService.getPermissionsByIds(permId);
                role.getPermissions().addAll(permissions);
                roleRepository.save(role);
                permissionService.save(
                        permissions.stream().map(permission -> {
                                permission.getRoles().add(role);
                                return permission;
                        }).toList()
                );

                return DataResponseUtils.successResponse("Permissions successfully added");
        }

        @Override
        public ApiDataResponseDto removeRolePermission(List<Long> permId, Long roleId) {
                Role role = roleRepository.findById(roleId).orElseThrow();
                List<Permission> permissions = permissionService.getPermissionsByIds(permId);
                role.getPermissions().removeAll(permissions);
                roleRepository.save(role);
                permissionService.save(
                        permissions.stream().map(permission -> {
                                permission.getRoles().remove(role);
                                return permission;
                        }).toList()
                );
                return DataResponseUtils.successResponse("Permissions successfully removed");
        }

        @Override
        public Optional<Role> getRoleById(Long id) {
                return roleRepository.findById(id);
        }

        @Override
        public Role getBusinessRoleByName(String name) {
                return null;
        }

        @Override
        public List<RoleDto> getBusinessDefaultRoles() {
                return roleRepository.findBusinessRolesWithoutCreatedBy().stream()
                        .map(role -> new RoleDto(role.getRoleType().name(), role.getName(), role.getId()))
                        .collect(Collectors.toList());
        }
}

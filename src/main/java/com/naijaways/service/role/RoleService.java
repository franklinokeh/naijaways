package com.naijaways.service.role;

import com.naijaways.entity.Role;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.CreateRoleDto;
import com.naijaways.models.RoleDto;

import java.util.List;
import java.util.Optional;

public interface RoleService {
        ApiDataResponseDto getBusinessRole(Long businessId);

        ApiDataResponseDto getAdminRoles();

        Role createAdminRole(CreateRoleDto createRoleDto);

        Role createBusinessRole(Long businessId, CreateRoleDto createRoleDto);

        ApiDataResponseDto addPermissionToRole(List<Long> permId, Long roleId);

        ApiDataResponseDto removeRolePermission(List<Long> permId, Long roleId);

        Optional<Role> getRoleById(Long id);

        Role getBusinessRoleByName(String name);

        List<RoleDto> getBusinessDefaultRoles();
}

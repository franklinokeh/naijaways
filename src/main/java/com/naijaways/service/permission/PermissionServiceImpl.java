package com.naijaways.service.permission;



import com.naijaways.entity.Business;
import com.naijaways.entity.Permission;
import com.naijaways.entity.Role;
import com.naijaways.enums.PermissionCategory;
import com.naijaways.enums.PermissionType;
import com.naijaways.enums.RoleType;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.CategoryPermissionsDto;
import com.naijaways.models.PermissionDto;
import com.naijaways.repository.BusinessRepository;
import com.naijaways.repository.PermissionRepository;
import com.naijaways.repository.RoleRepository;
import com.naijaways.utils.DataResponseUtils;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class PermissionServiceImpl implements PermissionService {
        PermissionRepository permissionsRepository;
        RoleRepository roleRepository;
        BusinessRepository businessRepository;

        public PermissionServiceImpl(PermissionRepository permissionsRepository, RoleRepository roleRepository, BusinessRepository businessRepository) {
                this.permissionsRepository = permissionsRepository;
                this.roleRepository = roleRepository;
                this.businessRepository = businessRepository;
        }


        @Override
        public ApiDataResponseDto getAdminPermissions() {
                List<Permission> adminPermissions = permissionsRepository.findAllByPermissionTypeIn(List.of(PermissionType.ADMIN, PermissionType.GLOBAL));
                return DataResponseUtils.successResponse("Successfully retrieved admin permissions", toCategoryPermissionsDto(adminPermissions));
        }

        @Override
        public ApiDataResponseDto getBusinessPermissions() {
                List<Permission> businessPermissions = permissionsRepository.findAllByPermissionTypeIn(List.of(PermissionType.BUSINESS, PermissionType.GLOBAL));
                return DataResponseUtils.successResponse("Successfully retrieved business permissions", toCategoryPermissionsDto(businessPermissions));
        }

        @Override
        public Collection<Permission> save(Collection<Permission> p) {
                return permissionsRepository.saveAll(p);
        }

        @Override
        public List<Permission> getPermissionsByIds(List<Long> permissionIds) {
                return permissionsRepository.findAllById(permissionIds);
        }

        Collection<CategoryPermissionsDto> toCategoryPermissionsDto(Collection<Permission> permissions) {
                Map<PermissionCategory, CategoryPermissionsDto> dtoHashMap = new HashMap<>();
                permissions.forEach(permission -> {
                        PermissionDto dto = PermissionDto.builder().name(permission.getName().toString())
                                .description(permission.getName().getDescription())
                                .category(permission.getCategory().toString())
                                .id(permission.getId()).build();

                        CategoryPermissionsDto categoryPermissionsDto = dtoHashMap.getOrDefault(permission.getCategory(), new CategoryPermissionsDto());
                        categoryPermissionsDto.setCategory(permission.getCategory().toString());
                        if (Objects.isNull(categoryPermissionsDto.getPermissions()))
                                categoryPermissionsDto.setPermissions(new ArrayList<>());

                        categoryPermissionsDto.getPermissions().add(dto);
                        dtoHashMap.put(permission.getCategory(), categoryPermissionsDto);
                });
                return dtoHashMap.values();
        }

        @Override
        public List<Permission> getPermissionsByRoles(Role role) {
                return permissionsRepository.findPermissionsByRoles(role);
        }

        @Override
        public ApiDataResponseDto getAdminPermissionsByRole(Long roleId) {
                Optional<Role> role = roleRepository.findById(roleId);
                if(role.isPresent() && role.get().getRoleType().equals(RoleType.ADMIN)){
                        return DataResponseUtils.successResponse("Successfully retrieved Admin permissions",
                                toCategoryPermissionsDto(permissionsRepository.findPermissionsByRoles(role.get())));
                }
                return DataResponseUtils.errorResponse(HttpStatus.NOT_FOUND, "Invalid Admin role");
        }

        @Override
        public ApiDataResponseDto getBusinessPermissionsByRole(Long businessId, Long roleId) {
                Optional<Business> business = businessRepository.findById(businessId);
                if(business.isPresent()){
                        Optional<Role> role = roleRepository.findByIdAndBusinessId(roleId, businessId);
                        if(role.isPresent()){
                                return DataResponseUtils.successResponse("Successfully retrieved Business permissions" +
                                                " for " + role.get().getName(), toCategoryPermissionsDto(permissionsRepository.findPermissionsByRoles(role.get())));
                        }
                        return DataResponseUtils.errorResponse(HttpStatus.NOT_FOUND, "Invalid Business Role");
                }
                return DataResponseUtils.errorResponse(HttpStatus.NOT_FOUND, "Invalid Business");
        }


}

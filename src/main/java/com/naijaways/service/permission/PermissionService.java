package com.naijaways.service.permission;

import com.naijaways.entity.Permission;
import com.naijaways.entity.Role;
import com.naijaways.models.ApiDataResponseDto;

import java.util.Collection;
import java.util.List;

public interface PermissionService {
        ApiDataResponseDto getAdminPermissions();

        ApiDataResponseDto getBusinessPermissions();

        Collection<Permission> save(Collection<Permission> p);

        List<Permission> getPermissionsByIds(List<Long> permissionIds);

        List<Permission> getPermissionsByRoles(Role role);

        ApiDataResponseDto getAdminPermissionsByRole(Long roleId);
        ApiDataResponseDto getBusinessPermissionsByRole(Long businessId, Long roleId);
}

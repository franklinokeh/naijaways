package com.naijaways.service.business;

import com.naijaways.entity.BusinessUser;
import com.naijaways.models.BusinessProfileDto;
import com.naijaways.models.CreateBusinessDto;
import com.naijaways.models.UpdateBusinessDto;

import java.util.List;

public interface BusinessService {
        BusinessUser createBusiness(CreateBusinessDto createBusinessDto);


        BusinessUser updateBusiness(Long businessId, UpdateBusinessDto updateBusinessDto);

        BusinessProfileDto getBusiness(Long businessId);

        List<BusinessProfileDto> getAllBusiness(Integer page, Integer pageSize);
}

package com.naijaways.service.business;

import com.naijaways.config.NaijaWaysConstants;
import com.naijaways.entity.Business;
import com.naijaways.entity.BusinessUser;
import com.naijaways.entity.Role;
import com.naijaways.entity.User;
import com.naijaways.enums.RoleType;
import com.naijaways.enums.UserType;
import com.naijaways.exception.BusinessAlreadyExistException;
import com.naijaways.exception.ResourceNotFoundException;
import com.naijaways.exception.UserAlreadyExistException;
import com.naijaways.models.BusinessProfileDto;
import com.naijaways.models.CreateBusinessDto;
import com.naijaways.models.PageableDto;
import com.naijaways.models.UpdateBusinessDto;
import com.naijaways.repository.*;
import com.naijaways.service.user.UserService;
import com.naijaways.utils.Misc;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@AllArgsConstructor
public class BusinessServiceImpl implements BusinessService{

        private final BusinessRepository businessRepository;

        private final BusinessUserEntityManagerRepo businessUserEntityManagerRepo;
        private final BusinessUserRepository businessUserRepository;
        private final PasswordEncoder passwordEncoder;
        private final UserRepository userRepository;
        private final RoleRepository roleRepository;
        private final UserService userService;

        @Override
        public BusinessUser createBusiness(CreateBusinessDto createBusinessDto) {
                userService.emailValidation(createBusinessDto.getBusinessUser().getUser().getEmail());
                Optional<Role> role =
                        roleRepository.findByNameAndRoleType(NaijaWaysConstants.BUSINESS_ADMIN_DEFAULT_ROLE, RoleType.BUSINESS);

                User userRetrieved = userRepository.findUserByEmail(createBusinessDto.getBusinessUser().getUser().getEmail());
                Optional<Business> businessRetrieved = businessRepository.findBusinessByName(createBusinessDto.getName());
                if (businessRetrieved.isEmpty()){
                        if (ObjectUtils.isEmpty(userRetrieved)){
                                if (role.isPresent()){
                                        boolean isValid =
                                                Misc.phoneNumberValidation(createBusinessDto.getBusinessUser().getUser().getPhoneNumber());
                                        if (BooleanUtils.isTrue(isValid)) {
                                                User user = new User();
                                                user.setEmail(createBusinessDto.getBusinessUser().getUser().getEmail());
                                                user.setUserType(UserType.BUSINESS);
                                                user.setPhoneNumber(createBusinessDto.getBusinessUser().getUser().getPhoneNumber());
                                                user.setAccountVerified(false);
                                                user.setAccountNonLocked(true);
                                                user.setUsername(createBusinessDto.getBusinessUser().getUser().getEmail());
                                                user.setPassword(passwordEncoder.encode(createBusinessDto.getBusinessUser().getUser().getPassword()));
                                                user.setAccountVerified(true);
                                                user.setAccountNonLocked(true);

                                                User userSaved = userRepository.save(user);

                                                Business business = new Business();
                                                business.setName(createBusinessDto.getName());
                                                business.setLogo(createBusinessDto.getLogo());
                                                business.setDescription(createBusinessDto.getDescription());
                                                business.setAddress(createBusinessDto.getAddress());
                                                business.setBusinessEnabled(userSaved.isAccountNonLocked());
                                                Business businessSaved = businessRepository.save(business);
                                                BusinessUser businessUser = new BusinessUser();
                                                businessUser.setFirstName(createBusinessDto.getBusinessUser().getFirstName());
                                                businessUser.setLastName(createBusinessDto.getBusinessUser().getLastName());
                                                businessUser.setBusiness(businessSaved);
                                                businessUser.setUser(userSaved);
                                                businessUser.setRole(role.get());

                                                return businessUserRepository.save(businessUser);
                                        }else throw  new RuntimeException(createBusinessDto.getBusinessUser().getUser().getPhoneNumber() +
                                                " is not a valid phone number");

                                }else throw new ResourceNotFoundException("Business admin role not found");

                        }else throw new UserAlreadyExistException("email already exist");

                }else throw new BusinessAlreadyExistException("Business name already exist");
        }

        @Override
        public BusinessUser updateBusiness(Long businessId, UpdateBusinessDto updateBusinessDto) {
                return null;
        }

        @Override
        public BusinessProfileDto getBusiness(Long businessId) {
                BusinessProfileDto businessProfileDto = new BusinessProfileDto();
                Optional<BusinessUser> businessUser = businessUserRepository
                        .getBusinessUserByBusinessIdAndRoleName(businessId, NaijaWaysConstants.BUSINESS_ADMIN_DEFAULT_ROLE);
                if (businessUser.isPresent()) {
                        businessProfileDto.setId(businessUser.get().getBusiness().getId());
                        businessProfileDto.setName(businessUser.get().getBusiness().getName());
                        businessProfileDto.setAddress(businessUser.get().getBusiness().getAddress());
                        businessProfileDto.setEmail(businessUser.get().getUser().getEmail());
                        businessProfileDto.setPhone(businessUser.get().getUser().getPhoneNumber());
                        businessProfileDto.setFirstName(businessUser.get().getFirstName());
                        businessProfileDto.setLastName(businessUser.get().getLastName());
                        businessProfileDto.setStatus(businessUser.get().getBusiness().isBusinessEnabled());
                        return businessProfileDto;
                }
                return null;
        }

        @Override
        public List<BusinessProfileDto> getAllBusiness(Integer page, Integer pageSize) {
                List<BusinessProfileDto> businessProfileDtos = new ArrayList<>();
                PageableDto pageableDto = new PageableDto();
                pageableDto.setPage(page);
                pageableDto.setPageSize(pageSize);
                Optional<Role> role =
                        roleRepository.findByNameAndRoleType(NaijaWaysConstants.BUSINESS_ADMIN_DEFAULT_ROLE, RoleType.BUSINESS);
                if (role.isPresent()){
                        List<BusinessUser> businessUsers = businessUserEntityManagerRepo.getAllBusinessUsersAdmin(pageableDto, role.get());
                        if (ObjectUtils.isNotEmpty(businessUsers)){
                                for (BusinessUser businessUser : businessUsers){
                                        BusinessProfileDto businessProfileDto = new BusinessProfileDto();
                                        businessProfileDto.setId(businessUser.getBusiness().getId());
                                        businessProfileDto.setName(businessUser.getBusiness().getName());
                                        businessProfileDto.setAddress(businessUser.getBusiness().getAddress());
                                        businessProfileDto.setEmail(businessUser.getUser().getEmail());
                                        businessProfileDto.setPhone(businessUser.getUser().getPhoneNumber());
                                        businessProfileDto.setFirstName(businessUser.getFirstName());
                                        businessProfileDto.setLastName(businessUser.getLastName());
                                        businessProfileDto.setStatus(businessUser.getBusiness().isBusinessEnabled());
                                        businessProfileDto.setStatus(businessUser.getUser().isAccountNonLocked());
                                        businessProfileDtos.add(businessProfileDto);
                                }
                        }else throw new ResourceNotFoundException("Business not found");

                }else throw new ResourceNotFoundException("Business admin role not found");

                return businessProfileDtos;
        }
}

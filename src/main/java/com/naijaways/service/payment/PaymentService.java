package com.naijaways.service.payment;

import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.PageableDto;
import org.springframework.http.ResponseEntity;

public interface PaymentService {
        ResponseEntity<ApiDataResponseDto> getPayments(PageableDto pageable);
}

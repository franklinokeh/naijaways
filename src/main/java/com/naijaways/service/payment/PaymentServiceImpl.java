package com.naijaways.service.payment;

import com.naijaways.entity.Payment;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.PageableDto;
import com.naijaways.repository.PaymentRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
@Slf4j
public class PaymentServiceImpl implements PaymentService{
        private final PaymentRepository paymentRepository;

        @Override
        public ResponseEntity<ApiDataResponseDto> getPayments(PageableDto pageable) {
                Pageable paging = PageRequest.of(pageable.getPage(),pageable.getPageSize());
                Page<Payment> paymentPage = paymentRepository.findAll(paging);
                if (paymentPage.isEmpty()) {
                        ApiDataResponseDto apiDataResponseDto =  new ApiDataResponseDto(true, 200,
                                paymentPage.toList(), "Successfully " +
                                "retrieved payments", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.NOT_FOUND);
                }
                ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, paymentPage, "Payments were" +
                        " not found", LocalDateTime.now().toString());
                return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
        }
}

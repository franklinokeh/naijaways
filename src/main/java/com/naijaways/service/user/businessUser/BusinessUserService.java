package com.naijaways.service.user.businessUser;

import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.UserDto;
import com.naijaways.models.UserRegisterDto;
import org.springframework.transaction.annotation.Transactional;

public interface BusinessUserService {
        @Transactional
        ApiDataResponseDto createBusinessUser(UserRegisterDto userDto);

        ApiDataResponseDto updateBusinessUser(UserDto userDto);

        ApiDataResponseDto getBusinessUsers();

}

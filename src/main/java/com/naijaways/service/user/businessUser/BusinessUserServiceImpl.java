package com.naijaways.service.user.businessUser;

import com.naijaways.entity.Business;
import com.naijaways.entity.BusinessUser;
import com.naijaways.entity.Role;
import com.naijaways.entity.User;
import com.naijaways.enums.UserType;
import com.naijaways.exception.AuthException;
import com.naijaways.exception.EntityConflictException;
import com.naijaways.exception.ResourceNotFoundException;
import com.naijaways.exception.UserAlreadyExistException;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.GetUserDto;
import com.naijaways.models.UserDto;
import com.naijaways.models.UserRegisterDto;
import com.naijaways.repository.BusinessUserRepository;
import com.naijaways.service.role.RoleService;
import com.naijaways.service.user.UserService;
import com.naijaways.utils.DataResponseUtils;
import com.naijaways.utils.EmailTemplateSender;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Service
@Slf4j
public class BusinessUserServiceImpl implements BusinessUserService {

        UserService userService;
        BusinessUserRepository businessUserRepository;

        PasswordEncoder passwordEncoder;
        RoleService roleService;

        EmailTemplateSender templateSender;

        @Transactional
        @Override
        public ApiDataResponseDto createBusinessUser(UserRegisterDto userDto) {
                if (userService.existsByEmail(userDto.getEmail()))
                        throw new UserAlreadyExistException("User already registered");
                Role role = roleService.getRoleById(userDto.getRoleId()).orElseThrow(() -> new ResourceNotFoundException("Selected role does not  exists"));
                Business business = userService.getLoggedInUser().getBusinessUser().getBusiness();
                if (Objects.isNull(business)) throw new ResourceNotFoundException("Not a registered business");
                if (!role.getBusiness().equals(business))
                        throw new EntityConflictException("Role does not belong to business");
                User savedUser = userService.save(dtoToUser(userDto));
                BusinessUser businessUser = businessUserRepository.save(dtoToUser(userDto, business, role, savedUser));
                templateSender.sendUserInviteMail(savedUser);
                return DataResponseUtils.successResponse(businessUser.getFirstName()+ " " + businessUser.getLastName() + "User " +
                        "successfully invited " .concat(business.getName()));
        }

        @Override
        public ApiDataResponseDto updateBusinessUser(UserDto userDto) {
                User user = userService.getLoggedInUser();
                if (ObjectUtils.notEqual(user.getUserType(), UserType.BUSINESS) || Objects.isNull(user.getBusinessUser()))
                        throw new AuthException("Full authorisation is required to view this resource");

                BusinessUser businessUser = user.getBusinessUser();
                businessUser.setFirstName(userDto.getFirstName());
                businessUser.setLastName(userDto.getLastName());

                businessUserRepository.save(businessUser);

                UserDto data = UserDto.builder()
                        .id(user.getId())
                        .userType(user.getUserType().name())
                        .firstName(businessUser.getFirstName())
                        .lastName(businessUser.getLastName())
                        .email(user.getEmail())
                        .enabled(user.isAccountVerified())
                        .build();

                return DataResponseUtils.successResponse("User updated", data);
        }

        @Override
        public ApiDataResponseDto getBusinessUsers() {
                User user = userService.getLoggedInUser();
                if (ObjectUtils.notEqual(user.getUserType(), UserType.BUSINESS) || Objects.isNull(user.getBusinessUser()))
                        throw new AuthException("Full authorisation is required to view this resource");

                Business business = user.getBusinessUser().getBusiness();
                List<GetUserDto> businessUsers = businessUserRepository.findAllByBusiness(business).stream().map(
                        businessUser -> GetUserDto.builder()
                                .id(businessUser.getUser().getId())
                                .userType(businessUser.getUser().getUserType().name())
                                .firstName(businessUser.getFirstName())
                                .lastName(businessUser.getLastName())
                                .email(businessUser.getUser().getEmail())
                                .phoneNumber(businessUser.getUser().getPhoneNumber())
                                .roleName(businessUser.getRole().getName())
                                .enabled(businessUser.getUser().isAccountVerified())
                                .build()
                ).toList();

                return DataResponseUtils.successResponse(String.format("%s users retrieved", business.getName()), businessUsers);
        }

        private User dtoToUser(UserRegisterDto dto) {
                User userToRegister = new User();
                userToRegister.setUserType(UserType.BUSINESS);
                userToRegister.setEmail(dto.getEmail());
                userToRegister.setPassword(passwordEncoder.encode(""));
                userToRegister.setAccountVerified(false);
                userToRegister.setPhoneNumber(dto.getPhonenumber());
                return userToRegister;
        }

        private BusinessUser dtoToUser(UserRegisterDto dto, Business business, Role role, User user) {
                BusinessUser businessUser = new BusinessUser();
                businessUser.setUser(user);
                businessUser.setFirstName(dto.getFirstName());
                businessUser.setLastName(dto.getLastName());
                businessUser.setBusiness(business);
                businessUser.setRole(role);
                return businessUser;
        }

}

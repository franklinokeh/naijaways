package com.naijaways.service.user.adminUser;


import com.naijaways.entity.Admin;
import com.naijaways.entity.Role;
import com.naijaways.entity.User;
import com.naijaways.enums.UserType;
import com.naijaways.exception.AuthException;
import com.naijaways.exception.ResourceNotFoundException;
import com.naijaways.exception.UserAlreadyExistException;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.GetUserDto;
import com.naijaways.models.UserDto;
import com.naijaways.models.UserRegisterDto;
import com.naijaways.repository.AdminRepository;
import com.naijaways.service.role.RoleService;
import com.naijaways.service.user.UserService;
import com.naijaways.utils.DataResponseUtils;
import com.naijaways.utils.EmailTemplateSender;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class AdminUserServiceImpl implements AdminUserService {

        UserService userService;
        AdminRepository adminRepository;

        PasswordEncoder passwordEncoder;
        RoleService roleService;

        EmailTemplateSender templateSender;

        @Transactional
        @Override
        public ApiDataResponseDto createAdminUser(UserRegisterDto userDto) {

                User loggedInUser = userService.getLoggedInUser();
                if (Objects.nonNull(loggedInUser.getBusinessUser()) || ObjectUtils.notEqual(loggedInUser.getUserType(), UserType.ADMIN))
                        throw new AuthException("Unauthorised user");

                if (userService.existsByEmail(userDto.getEmail()))
                        throw new UserAlreadyExistException("User already registered");
                Role role = roleService.getRoleById(userDto.getRoleId()).orElseThrow(() -> new ResourceNotFoundException("Selected role does not  exists"));


                User savedUser = userService.save(dtoToUser(userDto));

                adminRepository.save(dtoToUser(userDto, role, savedUser));

                templateSender.sendUserInviteMail(savedUser);

                return DataResponseUtils.successResponse("User successfully invited");
        }

        @Override
        public ApiDataResponseDto getAdminUsers() {

                if (ObjectUtils.notEqual(userService.getLoggedInUser().getUserType(), UserType.ADMIN))
                        throw new AuthException("Full authorisation is required to view this resource");

                List<GetUserDto> adminUsers = adminRepository.findAll()
                        .stream().map(
                                admin -> GetUserDto.builder()
                                        .id(admin.getUser().getId())
                                        .userType(admin.getUser().getUserType().name())
                                        .firstName(admin.getFirstName())
                                        .lastName(admin.getLastName())
                                        .email(admin.getUser().getEmail())
                                        .phoneNumber(admin.getUser().getPhoneNumber())
                                        .roleName(admin.getRole().getName())
                                        .enabled(admin.getUser().isAccountVerified())
                                        .build()
                        ).toList();

                return DataResponseUtils.successResponse("User successfully retrieved", adminUsers);
        }

        private User dtoToUser(UserRegisterDto dto) {
                User userToRegister = new User();
                userToRegister.setUserType(UserType.ADMIN);
                userToRegister.setEmail(dto.getEmail());
                userToRegister.setPassword(passwordEncoder.encode(""));
                userToRegister.setAccountVerified(false);
                userToRegister.setPhoneNumber(dto.getPhonenumber());
                return userToRegister;
        }

        @Override
        public ApiDataResponseDto updateAdminUser(UserDto userDto) {
                User user = userService.getLoggedInUser();
                if (ObjectUtils.notEqual(user.getUserType(), UserType.ADMIN) || Objects.isNull(user.getAdminUser()))
                        throw new AuthException("Full authorisation is required to view this resource");

                Admin admin = user.getAdminUser();
                admin.setLastName(userDto.getLastName());
                admin.setFirstName(userDto.getFirstName());

                adminRepository.save(admin);

                UserDto data = UserDto.builder()
                        .id(user.getId())
                        .userType(user.getUserType().name())
                        .firstName(admin.getFirstName())
                        .lastName(admin.getLastName())
                        .email(user.getEmail())
                        .enabled(user.isAccountVerified())
                        .build();

                return DataResponseUtils.successResponse("User updated", data);
        }

        private Admin dtoToUser(UserRegisterDto dto, Role role, User user) {
                Admin admin = new Admin();
                admin.setUser(user);
                admin.setFirstName(dto.getFirstName());
                admin.setLastName(dto.getLastName());
                admin.setRole(role);
                return admin;
        }
}

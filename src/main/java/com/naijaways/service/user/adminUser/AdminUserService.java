package com.naijaways.service.user.adminUser;

import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.UserDto;
import com.naijaways.models.UserRegisterDto;
import org.springframework.transaction.annotation.Transactional;

public interface AdminUserService {
        @Transactional
        ApiDataResponseDto createAdminUser(UserRegisterDto userDto);

        ApiDataResponseDto getAdminUsers();

        ApiDataResponseDto updateAdminUser(UserDto userDto);
}

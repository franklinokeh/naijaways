package com.naijaways.service.user;

import com.naijaways.entity.User;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.LoginResponseDto;
import com.naijaways.models.UserDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

        User registerUser(User user);

        void emailValidation(String email);

        void naijaWaysEmailValidation(String email);

        User enableUser(String username);

        User disableUser(String username);

        Page<User> getAllUsers(Pageable paging);

        UserDto userToUserDto(User user);

        User getUserByEmail(String email);

        boolean existsByEmail(String email);

        User getLoggedInUser();

        ApiDataResponseDto getLoggedInUserDetail(String token);

        User save(User entity);

        LoginResponseDto getLoginRequestDto(String email, String token);
}

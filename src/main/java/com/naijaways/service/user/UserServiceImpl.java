package com.naijaways.service.user;

import com.naijaways.config.securityConfig.JWTUtility;
import com.naijaways.entity.Admin;
import com.naijaways.entity.BusinessUser;
import com.naijaways.entity.Permission;
import com.naijaways.entity.User;
import com.naijaways.enums.UserType;
import com.naijaways.exception.ResourceNotFoundException;
import com.naijaways.exception.UserAlreadyExistException;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.GetUserDto;
import com.naijaways.models.LoginResponseDto;
import com.naijaways.models.UserDto;
import com.naijaways.repository.AdminRepository;
import com.naijaways.repository.BusinessUserRepository;
import com.naijaways.repository.PermissionRepository;
import com.naijaways.repository.UserRepository;
import com.naijaways.utils.DataResponseUtils;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

        private final PasswordEncoder passwordEncoder;

        private final UserRepository userRepository;
        private final AdminRepository adminRepository;
        private final BusinessUserRepository businessUserRepository;
        private final PermissionRepository permissionsRepository;
        private final JWTUtility jwtUtility;

        @Override
        public User registerUser(User user){
                User userSaved = userRepository.findUserByEmail(user.getEmail());
                if (ObjectUtils.isNotEmpty(userSaved)){
                        throw new UserAlreadyExistException("username already existed");
                }
                naijaWaysEmailValidation(user.getEmail());
                return userRepository.save(user);
        }

        @Override
        public void emailValidation(String email){
                if (BooleanUtils.isFalse(EmailValidator.getInstance().isValid(email))) {
                        throw new RuntimeException("Invalid e-mail supplied");
                }
        }

        @Override
        public void naijaWaysEmailValidation(String email){
                String emailRegExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "(naijaways\\.com)$";

                if (BooleanUtils.isFalse(EmailValidator.getInstance().isValid(email))) {
                        throw new RuntimeException("Invalid e-mail supplied");
                }
                Pattern emailPattern = Pattern.compile(emailRegExpn, Pattern.CASE_INSENSITIVE);
                Matcher emailMatcher = emailPattern.matcher(email);
                if (BooleanUtils.isFalse(emailMatcher.matches())) {
                        throw new RuntimeException("Non-Naijaways email supplied");
                }
        }

        @Override
        public User enableUser(String username) {

                User user = userRepository.findUserByEmail(username);

                if (ObjectUtils.isEmpty(user)){
                        throw new ResourceNotFoundException("user not found");
                }
                if (user.isAccountVerified()){
                        throw new RuntimeException("User already enabled");
                }

                user.setAccountVerified(true);

                return userRepository.save(user);
        }

        @Override
        public User disableUser(String username) {

                User user = userRepository.findUserByEmail(username);
                if (ObjectUtils.isEmpty(user)){
                        throw new ResourceNotFoundException("user not found");
                }
                if (!user.isAccountVerified()){
                        throw new RuntimeException("User already disabled");
                }
                user.setAccountVerified(false);
                return userRepository.save(user);
        }

        @Override
        public Page<User> getAllUsers(Pageable paging) {
                return userRepository.findAll(paging);
        }

        @Override
        public UserDto userToUserDto(User user){
                UserDto userDto = new UserDto();
                userDto.setEmail(user.getEmail());
                userDto.setEnabled(user.isAccountVerified());

                return userDto;
        }


        @Override
        public User getUserByEmail(String email) {
                return Optional.ofNullable(userRepository.findUserByEmail(email))
                        .orElseThrow(()-> new EntityNotFoundException(String.format("User with email %s does not exist", email)));
        }

        @Override
        public boolean existsByEmail(String email) {
                return userRepository.existsByEmail(email);
        }

        @Override
        public User getLoggedInUser(){
                Authentication authentication = Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                        .orElseThrow(() -> new EntityNotFoundException("You must be logged in to perform this operation"));;
                return Optional.ofNullable(userRepository.findUserByEmail(authentication.getName()))
                        .orElseThrow(() -> new EntityNotFoundException("You must be logged in to perform this operation"));
        }

        @Override
        public ApiDataResponseDto getLoggedInUserDetail(final String token) {
                final String email = jwtUtility.getUserEmailFromToken(token);

                User user = Optional.ofNullable(userRepository.findUserByEmail(email))
                        .orElseThrow(() -> new EntityNotFoundException("Unknown user"));
                boolean isAdmin = Objects.equals(UserType.ADMIN, user.getUserType());
                GetUserDto userDto = GetUserDto.builder()
                        .id(user.getId())
                        .userType(user.getUserType().name())
                        .firstName(isAdmin ? user.getAdminUser().getFirstName() : user.getBusinessUser().getFirstName())
                        .lastName(isAdmin ? user.getAdminUser().getLastName() : user.getBusinessUser().getLastName())
                        .email(user.getEmail())
                        .phoneNumber(user.getPhoneNumber())
                        .roleName(isAdmin ? user.getAdminUser().getRole().getName() : user.getBusinessUser().getRole().getName())
                        .enabled(user.isAccountVerified())
                        .build();

                return DataResponseUtils.successResponse("User details successfully retrieved", userDto);
        }
        @Override
        public User save(User entity){
                return userRepository.save(entity);
        }

        @Override
        public LoginResponseDto getLoginRequestDto(String email, String token){
                LoginResponseDto loginResponseDto = new LoginResponseDto();
                List<String> permissionNames = new ArrayList<>();
                User user = userRepository.findUserByEmail(email);
                if (user.getUserType().equals(UserType.ADMIN)){
                        Admin admin = adminRepository.findAdminByUserId(user.getId());
                        List<Permission> permissions = permissionsRepository.findPermissionsByRoles(admin.getRole());
                        permissionNames = getPermissionNames(permissions);
                        loginResponseDto.setId(user.getId());
                        loginResponseDto.setEmail(user.getEmail());
                        loginResponseDto.setAccessToken(token);
                        loginResponseDto.setRoleName(admin.getRole().getName());
                        loginResponseDto.setRoleType(admin.getRole().getRoleType().name());
                        loginResponseDto.setBusinessName("none");
                        loginResponseDto.setPermissions(permissionNames);
                }else{
                        BusinessUser businessUser = businessUserRepository.findBusinessUserByUserId(user.getId());
                        List<Permission> permissions = permissionsRepository.findPermissionsByRoles(businessUser.getRole());
                        permissionNames = getPermissionNames(permissions);
                        loginResponseDto.setId(user.getId());
                        loginResponseDto.setEmail(user.getEmail());
                        loginResponseDto.setAccessToken(token);
                        loginResponseDto.setRoleName(businessUser.getRole().getName());
                        loginResponseDto.setRoleType(businessUser.getRole().getRoleType().name());
                        loginResponseDto.setBusinessName(businessUser.getBusiness().getName());
                        loginResponseDto.setPermissions(permissionNames);
                }

                return loginResponseDto;
        }

        public List<String> getPermissionNames(List<Permission> permissions){
                List<String> permissionName = new ArrayList<>();
                for (Permission permission : permissions){
                        permissionName.add(permission.getName().name());
                }
                return permissionName;
        }
}

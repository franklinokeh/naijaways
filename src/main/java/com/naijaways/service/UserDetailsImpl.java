package com.naijaways.service;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.naijaways.entity.Permission;
import com.naijaways.entity.Role;
import com.naijaways.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Slf4j
public class UserDetailsImpl implements UserDetails {
        private final Long id;
        private final String username;
        private final String email;
        @JsonIgnore
        private final String password;

        private final Collection<? extends GrantedAuthority> authorities;

        public UserDetailsImpl(Long id, String username, String email, String password, Collection<? extends GrantedAuthority> authorities) {
                this.id = id;
                this.username = username;
                this.email = email;
                this.password = password;
                this.authorities = authorities;
        }




        private Collection<? extends GrantedAuthority> getAuthorities(Collection<Role> roles) {
                return getGrantedAuthorities(getPermissions(roles));
        }

        private List<String> getPermissions(Collection<Role> roles) {
                List<String> permissions = new ArrayList<>();
                List<Permission> collection = new ArrayList<>();
                for (Role role : roles) {
                        permissions.add(role.getName());
                        collection.addAll(role.getPermissions());
                }
                for (Permission item : collection) {
                        permissions.add(String.valueOf(item.getName()));
                }
                log.info("getPermissions:::{}", permissions);
                return permissions;
        }

        private List<GrantedAuthority> getGrantedAuthorities(List<String> permissions) {
                List<GrantedAuthority> authorities = new ArrayList<>();
                for (String permission : permissions) {
                        authorities.add(new SimpleGrantedAuthority(permission));
                }
                log.info("getAuthorities:::{}", authorities);
                return authorities;
        }

        public static UserDetailsImpl build(User user) {
                List<GrantedAuthority> authorities = new ArrayList<>();
                return new UserDetailsImpl(
                        user.getId(),
                        user.getUsername(),
                        user.getEmail(),
                        user.getPassword(),
                        authorities);
        }


        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
                return authorities;
        }

        public Long getId() {
                return id;
        }

        public String getEmail() {
                return email;
        }

        @Override
        public String getPassword() {
                return this.password;
        }

        @Override
        public String getUsername() {
                return this.username;
        }

        @Override
        public boolean isAccountNonExpired() {
                return true;
        }

        @Override
        public boolean isAccountNonLocked() {
                return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
                return true;
        }

        @Override
        public boolean isEnabled() {
                return true;
        }
}

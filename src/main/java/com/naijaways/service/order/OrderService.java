package com.naijaways.service.order;

import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.CreateOrderDto;
import com.naijaways.models.SortOrdersDto;
import org.apache.tika.exception.TikaException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface OrderService {
        ResponseEntity<ApiDataResponseDto> createOrderByWaybill(MultipartFile file) throws TikaException, IOException;

        ResponseEntity<ApiDataResponseDto> createOrder(CreateOrderDto createOrderDto);

        ResponseEntity<ApiDataResponseDto> getAllOrders(SortOrdersDto pageable);
}

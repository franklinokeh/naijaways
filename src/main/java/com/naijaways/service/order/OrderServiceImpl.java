package com.naijaways.service.order;

import com.naijaways.entity.Order;
import com.naijaways.entity.Shipment;
import com.naijaways.entity.User;
import com.naijaways.enums.OrderStatus;
import com.naijaways.enums.ShipmentStatus;
import com.naijaways.exception.BadRequestException;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.CreateOrderDto;
import com.naijaways.models.SortOrdersDto;
import com.naijaways.repository.OrderRepository;
import com.naijaways.service.user.UserService;
import com.naijaways.utils.Misc;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@AllArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

        UserService userService;
        OrderRepository orderRepository;


        @Override
        public ResponseEntity<ApiDataResponseDto> createOrderByWaybill(MultipartFile file) throws TikaException, IOException {
                if(file.getSize() > 3000000) {
                        log.info("File size should be less than 3MB ====== {}", file.getSize());
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 400, new Object(),
                                "File size should be less than 3MB", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
                }
                File tempFile = File.createTempFile("temp", file.getOriginalFilename());
                file.transferTo(tempFile);
                try {
                        Tika tika = new Tika();
                        String text = tika.parseToString(tempFile);
                        log.info("Text extracted =======");
                        log.info(text);
                        String senderName = extractValue("(From|Sender): (.*)", text);
                        String quantity = extractValue("(Quantity|Total Qty): (.*)", text);
                        String costOfShipment = extractValue("(Price|Cost|Total Price): (.*)", text);
                        String goods = extractValue("(Description): (.*)", text);
                        String weight = extractValue("(Weight): (.*)", text);
                        String origin = extractValue("(Origin|From): (.*)", text);
                        String deliveryNote = extractValue("(Delivery note): (.*)", text);
                        String dimension = extractValue("(Dimension|Size): (.*)", text);
                        String destination = extractValue("(location|destination|address|deliver to): (.*)", text);
                        String recipient = extractValue("(To|Customer): (.*)", text);
                        String methodOfShipment = extractValue("(Method of shipment): (.*)", text);
                        String trackingNo = extractValue("(Tracking|Tracking#): (.*)", text);
                        CreateOrderDto createOrderDto = CreateOrderDto.builder()
                                .quantity(quantity)
                                .costOfShipment(costOfShipment)
                                .destination(destination)
                                .goods(goods)
                                .origin(origin)
                                .sender(senderName)
                                .deliveryNote(deliveryNote)
                                .weight(new BigDecimal(weight))
                                .dimension(dimension)
                                .destination(destination)
                                .recipient(recipient)
                                .methodOfShipment(methodOfShipment)
                                .trackingNo(trackingNo)
                                .build();
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, createOrderDto,
                                "File contents extracted ", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);

                } catch (IOException e) {
                        e.printStackTrace();
                        log.error("Error reading file .... {}", e.getMessage());
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(false, 400, new Object(),
                                "Error reading file", LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
                }
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> createOrder(CreateOrderDto createOrderDto) {
                User user = userService.getLoggedInUser();
                String trackingNo = Misc.generateRef();
                ///////////////////////////////////////////////////
                Shipment newShipment = new Shipment();
                newShipment.setCostOfShipment(new BigDecimal(createOrderDto.getCostOfShipment()));
                newShipment.setMethodOfShipment(createOrderDto.getMethodOfShipment());
                newShipment.setOrigin(createOrderDto.getOrigin());
                newShipment.setStatus(ShipmentStatus.CREATED);
                newShipment.setDestination(createOrderDto.getDestination());
                newShipment.setDimensions(createOrderDto.getDimension());
                newShipment.setSender(createOrderDto.getSender());
                newShipment.setRecipient(createOrderDto.getRecipient());
                newShipment.setWeight(createOrderDto.getWeight());
                newShipment.setDeliveryNote(createOrderDto.getDeliveryNote());
                newShipment.setReceiptNote(null);
                newShipment.setRecipientPhone(createOrderDto.getRecipientPhone());
                newShipment.setTrackingNo(trackingNo);
                //////////////////////////////////////////////////////
                List<Shipment> shipments = new ArrayList<>();
                shipments.add(newShipment);
                Order newOrder = new Order();
                newOrder.setStatus(OrderStatus.CREATED);
                newOrder.setBusiness(user.getBusinessUser().getBusiness());
                newOrder.setShipments(shipments);
                newShipment.setOrder(newOrder);
                try {
                        orderRepository.save(newOrder);
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, "Successfully " +
                                "created new order", newOrder.toString(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.OK);
                }catch (BadRequestException ex){
                        ApiDataResponseDto apiDataResponseDto = new ApiDataResponseDto(true, 200, "Successfully " +
                                "Exception in creating new order", newOrder.toString(), LocalDateTime.now().toString());
                        return new ResponseEntity<>(apiDataResponseDto, HttpStatus.BAD_REQUEST);
                }
        }

        @Override
        public ResponseEntity<ApiDataResponseDto> getAllOrders(SortOrdersDto pageable) {
                return null;
        }

        private String extractValue(String pattern, String text) {
                Pattern p = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
                Matcher m = p.matcher(text);
                if (m.find()) {
                        String extractedValue = m.group(2);
                        log.info("Value after keyword '" + pattern + "': " + extractedValue);
                        return extractedValue;
                } else {
                        log.info("Keyword '" + pattern + "' not found in text.");
                        return "";
                }
        }
}

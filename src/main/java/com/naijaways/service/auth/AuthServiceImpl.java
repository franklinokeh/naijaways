package com.naijaways.service.auth;


import com.naijaways.config.NaijaWaysConstants;
import com.naijaways.config.securityConfig.JWTUtility;
import com.naijaways.entity.User;
import com.naijaways.exception.AuthException;
import com.naijaways.exception.BadRequestException;
import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.ChangePasswordDto;
import com.naijaways.models.VerifyUserDto;
import com.naijaways.service.user.UserService;
import com.naijaways.utils.DataResponseUtils;
import com.naijaways.utils.EmailTemplateSender;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class AuthServiceImpl implements AuthService {
        UserService userService;
        JWTUtility jwtUtility;

        PasswordEncoder passwordEncoder;
        EmailTemplateSender templateSender;


        @Override
        public ApiDataResponseDto verifyUser(VerifyUserDto dto, String token) {
                if (BooleanUtils.isTrue(jwtUtility.isTokenExpired(token)))
                        throw new AuthException("Invalid verification token");

                if (BooleanUtils.isFalse(passwordValid.test(dto.getPassword())))
                        throw new BadRequestException("Weak password");
                String email = jwtUtility.getUserEmailFromToken(token);
                User user = userService.getUserByEmail(email);

                user.setPassword(passwordEncoder.encode(dto.getPassword()));
                user.setAccountVerified(true);

                userService.save(user);

                return DataResponseUtils.successResponse("User successfully verified");
        }

        @Override
        public ApiDataResponseDto changePassword(ChangePasswordDto changePasswordDto){
                User loggedInUser =  userService.getLoggedInUser();
                if (BooleanUtils.isFalse(passwordEncoder.matches(changePasswordDto.getOldPassword(),loggedInUser.getPassword())))
                        throw new AuthException("Invalid credentials");

                if (BooleanUtils.isFalse(passwordValid.test(changePasswordDto.getRequest().getPassword())))
                        throw new BadRequestException("Weak password");

                loggedInUser.setPassword(passwordEncoder.encode(changePasswordDto.getRequest().getPassword()));

                userService.save(loggedInUser);

                return DataResponseUtils.successResponse("Password successfully updated");
        }

        @Override
        public ApiDataResponseDto initiatePasswordReset(final String email) {
                User user = userService.getUserByEmail(email);
                templateSender.sendResetPasswordMail(user);

                return DataResponseUtils.successResponse("Password reset mail sent to: " + maskText(email));
        }

        private String maskText(String email) {
                int atIdx = email.indexOf("@");
                int chars = Math.max(atIdx - 3, 1);
                return email.replaceAll(String.format("\\S{1,%d}@", chars), "...@");
        }

        Predicate<String> passwordValid = password ->{
                Pattern pattern = Pattern.compile(NaijaWaysConstants.PASSWORD_PATTERN, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(password);
                return matcher.matches();
        };
}

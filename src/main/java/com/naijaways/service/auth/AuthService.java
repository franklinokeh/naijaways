package com.naijaways.service.auth;

import com.naijaways.models.ApiDataResponseDto;
import com.naijaways.models.ChangePasswordDto;
import com.naijaways.models.VerifyUserDto;

public interface AuthService {
        ApiDataResponseDto verifyUser(VerifyUserDto dto, String token);

    ApiDataResponseDto changePassword(ChangePasswordDto changePasswordDto);

    ApiDataResponseDto initiatePasswordReset(String email);
}

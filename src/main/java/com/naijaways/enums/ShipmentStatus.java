package com.naijaways.enums;

public enum ShipmentStatus {
        CREATED, IN_TRANSIT, AWAITING_PAYMENT, DELIVERED, CANCELLED
}

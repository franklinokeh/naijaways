package com.naijaways.enums;

public enum PermissionCategory {
        TRANSACTION, BUSINESS, GUEST, PERMISSION,
        CONFIGURATION, SETTINGS,
}

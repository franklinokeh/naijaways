package com.naijaways.enums;

public enum PermissionType {
        BUSINESS, ADMIN, GUEST, GLOBAL
}

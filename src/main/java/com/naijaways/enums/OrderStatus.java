package com.naijaways.enums;

public enum OrderStatus {
        CREATED, IN_TRANSIT, AWAITING_PAYMENT, DELIVERED, CANCELLED
}

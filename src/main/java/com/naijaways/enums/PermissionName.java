package com.naijaways.enums;

public enum PermissionName {
        // ==> GLOBAL PERMISSIONS

        TRANSACTION_ADD("Can perform transactions"),
        TRANSACTION_VIEW("Can view transactions"),
        TRANSACTION_DOWNLOAD("Can download transactions"),
        TRANSACTION_REPROCESS("Can reprocess transactions"),


        // ==> BUSINESS PERMISSIONS

        MERCHANT_ADD("Can add merchants"),
        MERCHANT_VIEW("Can view merchants"),
        MERCHANT_DELETE("Can delete merchants"),
        MERCHANT_EDIT("Can edit merchants"),
        MERCHANT_TOGGLE("Can enable/disable merchants"),
        MERCHANT_UPLOAD("Can upload merchants"),

        TERMINAL_ADD("Can add terminals"),
        TERMINAL_VIEW("Can view terminals"),
        TERMINAL_DELETE("Can delete terminals"),
        TERMINAL_EDIT("Can edit terminals"),
        TERMINAL_TOGGLE("Can enable/disable terminals"),
        TERMINAL_UPLOAD("Can upload terminals"),


        CONFIGURATION_SWITCH_PROVIDERS("Can switch Business providers"),
        CONFIGURATION_BIN_ROUTING("Can set BIN routing"),
        CONFIGURATION_AMOUNT("Can set amount"),


// ==> ADMIN PERMISSIONS


        PERMISSION_REMOVE_MEMBER("Can remove existing team members"),
        PERMISSION_CHANGE_ROLE("Can change team member's role"),
        PERMISSION_CREATE_ROLE("Can create roles"),
        PERMISSION_EDIT_ROLE("Can edit roles"),
        PERMISSION_INVITE_MEMBER("Can invite new team members"),
        PERMISSION_TOGGLE_MEMBER("Can enable/disable members"),

        BUSINESS_ADD("Can add businesses"),
        BUSINESS_VIEW("Can view businesses"),
        BUSINESS_DELETE("Can delete businesses"),
        BUSINESS_EDIT("Can edit businesses"),
        BUSINESS_TOGGLE("Can enable/disable businesses"),


        SETTINGS_BUSINESS_WEBHOOK_VIEW("Can view Business webhook"),
        SETTINGS_BUSINESS_WEBHOOK_UPDATE("Can update Business webhook"),
        SETTINGS_BUSINESS_WEBHOOK_TOGGLE("Can enable/disable Businesses webhooks"),
        SETTINGS_RESET_PASSWORD("Can reset members' password"), ADMIN_TRANSACTION_ADD(""), ADMIN_TRANSACTION_VIEW(""), ADMIN_TRANSACTION_DOWNLOAD(""), ADMIN_TRANSACTION_REPROCESS(""), ADMIN_SETTINGS_RESET_PASSWORD(""), ADMIN_PERMISSION_REMOVE_MEMBER(""), ADMIN_PERMISSION_CHANGE_ROLE(""), ADMIN_PERMISSION_CREATE_ROLE(""), ADMIN_PERMISSION_EDIT_ROLE(""), ADMIN_PERMISSION_INVITE_MEMBER(""), ADMIN_PERMISSION_TOGGLE_MEMBER("");


        String description;

        PermissionName(String description) {
        }

        public String getDescription() {
                return description;
        }
}

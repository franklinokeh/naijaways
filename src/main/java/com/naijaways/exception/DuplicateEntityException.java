package com.naijaways.exception;


public class DuplicateEntityException extends RuntimeException {
        public DuplicateEntityException(String message) {
                super(message);
        }

        public DuplicateEntityException() {
                super("Device already registered");
        }
}

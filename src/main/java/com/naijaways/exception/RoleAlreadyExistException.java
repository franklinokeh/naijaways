package com.naijaways.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class RoleAlreadyExistException extends RuntimeException {
        public RoleAlreadyExistException(String message) {
                super(message);
        }
}

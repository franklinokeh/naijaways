package com.naijaways.exception;


public class UnkwownDeviceException extends RuntimeException {
        public UnkwownDeviceException(String message) {
                super(message);
        }

        public UnkwownDeviceException() {
                super("Unknown device");
        }

}

package com.naijaways.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class MerchantAlreadyExistException extends RuntimeException {
        public MerchantAlreadyExistException() {
        }

        public MerchantAlreadyExistException(String message) {
                super(message);
        }
}

package com.naijaways.exception;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BusinessAlreadyExistException extends RuntimeException {
        public BusinessAlreadyExistException(String message) {
                super(message);
        }
}


package com.naijaways;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.naijaways.config.ConfigProperties;
import net.sourceforge.tess4j.Tesseract;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
//@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@EnableConfigurationProperties(ConfigProperties.class)
//@ComponentScan(basePackages = {"com.naijaways.service", "com.naijaways.utils", "com" +
//        ".naijaways.handler", "com.naijaways.config","net.sourceforge.tess4j"})
public class NaijaWaysBeApplication {


        @GetMapping("/")
        public String home() {
                return "Spring is king \uD83D\uDC4C ✌️";
        }

        @Bean
        public PasswordEncoder passwordEncoder() {
                return new BCryptPasswordEncoder();
        }

        @Bean
        public Tesseract tesseract() {
                return new Tesseract();
        }

        @Bean
        public RestTemplate restTemplate(RestTemplateBuilder builder) {
                return builder.build();
        }


        @Bean
        public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
                return new MappingJackson2HttpMessageConverter(mapper);
        }

//        @Bean
//        public CorsFilter corsFilter() {
//                UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//                CorsConfiguration config = new CorsConfiguration();
//                config.setAllowCredentials(true);
//                config.addAllowedOrigin("*");
//                config.addAllowedHeader("*");
//                config.addAllowedMethod("*");
//                source.registerCorsConfiguration("/**", config);
//                return new CorsFilter(source);
//        }

        public static void main(String[] args) {
                SpringApplication.run(NaijaWaysBeApplication.class, args);
        }

}
